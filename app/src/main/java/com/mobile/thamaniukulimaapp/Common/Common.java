package com.mobile.thamaniukulimaapp.Common;


import android.graphics.Color;
import android.graphics.Paint;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.auth.FirebaseAuth;
import com.mobile.thamaniukulimaapp.MainFragments.FarmingDetailedFragment;
import com.mobile.thamaniukulimaapp.R;

import java.text.SimpleDateFormat;
import java.util.Date;


public class Common {
    public static final String API_KEY = "617689354d86ec17722eaf684ea1341a";
    public static Location current_location = null;

    public static String getUserID(){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        return mAuth.getCurrentUser().getUid();
    }
    public static void openFarmingDetailed(View mView, double dbLat, double dbLng, String farmID, String crop, String activity){
        Bundle bundle = new Bundle();
        bundle.putDouble("lat", dbLat);
        bundle.putDouble("lng", dbLng);
        bundle.putString("farmID", farmID);
        bundle.putString("crop", crop);
        bundle.putString("activity", activity);
        FarmingDetailedFragment farmingDetailedFragment = new FarmingDetailedFragment();
        farmingDetailedFragment.setArguments(bundle);

        AppCompatActivity appCompatActivity = (AppCompatActivity) mView.getContext();
        FragmentTransaction transaction;
        transaction = appCompatActivity.getSupportFragmentManager().beginTransaction();
        transaction.addToBackStack(null);
        transaction.replace(R.id.frameLayout, farmingDetailedFragment);
        transaction.commit();
    }
    public static String convertUnixToDate(long dt) {
        Date date = new Date(dt*1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd EEE");
        return simpleDateFormat.format(date);
    }
    public static String convertUnixToDate2(long dt) {
        Date date = new Date(dt*1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm dd-MM-yy");
        return simpleDateFormat.format(date);
    }
    public static String convertUnixToDate3(long dt) {
        Date date = new Date(dt*1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yy");
        return simpleDateFormat.format(date);
    }

    public static String getTime(long dt){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMM yy");
        return simpleDateFormat.format(dt);

    }
    public static void checkedBox(TextView start_time, TextView end_time, TextView desc){
        start_time.setTextColor(Color.GRAY);
        end_time.setTextColor(Color.GRAY);
        desc.setTextColor(Color.GRAY);
        start_time.setPaintFlags(start_time.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        end_time.setPaintFlags(end_time.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        desc.setPaintFlags(desc.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

    }
    public static void uncheckedBox(TextView start_time, TextView end_time, TextView desc){
        start_time.setTextColor(Color.GRAY);
        end_time.setTextColor(Color.GRAY);
        desc.setTextColor(Color.parseColor("#23ce5c"));
        start_time.setPaintFlags(start_time.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        end_time.setPaintFlags(end_time.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
        desc.setPaintFlags(desc.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));

    }
    public static String convertUnixToHour(long dt) {
        Date date = new Date(dt*1000L);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        return simpleDateFormat.format(date);
    }
}
