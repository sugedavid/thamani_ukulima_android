package com.mobile.thamaniukulimaapp.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.Model.WeatherForecastResult;
import com.mobile.thamaniukulimaapp.R;
import com.squareup.picasso.Picasso;

import java.util.Calendar;
import java.util.UUID;

public class WeatherForecastAdapterB extends RecyclerView.Adapter<WeatherForecastAdapterB.MyViewHolder> {

    Context context;
    WeatherForecastResult weatherForecastResult;
    String fID, crop;
    private FirebaseAuth mAuth;
    private DatabaseReference mTaskDB;
    private TextView mTxtDesc, mTxtTemp, mTxtTime, mEdtTask;
    private ImageView imgCloud;
    private ImageButton imgBtnReminder;
    private DatabaseReference mWeatherDB;

    public WeatherForecastAdapterB(Context context, WeatherForecastResult weatherForecastResult, String fID, String crop) {
        this.context = context;
        this.weatherForecastResult = weatherForecastResult;
        this.fID = fID;
        this.crop = crop;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_weather_forecastb,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        String desc = weatherForecastResult.list.get(position)
                .weather.get(0).getDescription();


            //Load Image
            Picasso.get().load(new StringBuilder("http://openweathermap.org/img/w/")
                    .append(weatherForecastResult.list.get(position).weather.get(0).getIcon())
                    .append(".png").toString()).into(holder.img_weather);


            String time = Common.convertUnixToDate(weatherForecastResult
                    .list.get(position).dt);

            holder.txt_date_time.setText(time);
            holder.txt_description.setText(desc);

            holder.txt_temperature.setText(new StringBuilder(String.valueOf(weatherForecastResult.list.get(position)
                    .main.getTemp())).append("°C"));

            mAuth = FirebaseAuth.getInstance();
            String userID = mAuth.getCurrentUser().getUid();
            String rideID = UUID.randomUUID().toString();

            String temp = String.valueOf(weatherForecastResult.list.get(position)
                    .main.getTemp());

            // ------- POST WEATHER INFO ---------------
//            mWeatherDB = FirebaseDatabase.getInstance().getReference().child("Weather").child(userID)
//                    .child(String.valueOf(fID)).child(String.valueOf(position));
//            mWeatherDB.child("Desc").setValue(desc);
//            mWeatherDB.child("Time").setValue(time);
//            mWeatherDB.child("Temp").setValue(temp);


            mTaskDB = FirebaseDatabase.getInstance().getReference().child("Tasks").child(userID).child(rideID);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // create an alert builder
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle("New Task");

                    // set the custom layout
                    LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View customLayout;
                    if (li != null) {
                        customLayout = li.inflate(R.layout.task_layout, null);
                        mEdtTask = customLayout.findViewById(R.id.edtTask);
                        mEdtTask.setText("Land Preparation");
                        builder.setView(customLayout);
                        // add a button
                        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                // ----------------- POST TASK TO DB -----------------------

                                String task = mEdtTask.getText().toString().trim();
                                if (!task.isEmpty()){
                                    mTaskDB.child("Crop").setValue(crop);
                                    mTaskDB.child("Description").setValue(task);
                                    mTaskDB.child("Date").setValue(Common.convertUnixToDate2(weatherForecastResult
                                            .list.get(position).dt));
                                    mTaskDB.child("DateLng").setValue(weatherForecastResult
                                            .list.get(position).dt);
                                    mTaskDB.child("Status").setValue("Incomplete").addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()){
                                                Toast.makeText(context, "Task Added", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                                }else {
                                    Toast.makeText(context, "Enter Task", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });
                        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        });

                        // create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.show();


                        imgBtnReminder = customLayout.findViewById(R.id.imgReminderTask);
                        imgBtnReminder.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                Calendar cal = Calendar.getInstance();
                                Intent intent = new Intent(Intent.ACTION_EDIT);
                                intent.setType("vnd.android.cursor.item/event");
                                intent.putExtra("beginTime", weatherForecastResult
                                        .list.get(position).dt);
                                intent.putExtra("allDay", true);
                                intent.putExtra("rrule", "FREQ=YEARLY");
                                intent.putExtra("endTime", cal.getTimeInMillis()+60*60*1000);
                                intent.putExtra("title", "Add Task");
                                customLayout.getContext().startActivity(intent);

//                                Intent myIntent = new Intent(customLayout.getContext() , NotifyService. class ) ;
//                                AlarmManager alarmManager = (AlarmManager) customLayout.getContext().getSystemService( ALARM_SERVICE ) ;
//                                PendingIntent pendingIntent = PendingIntent. getService (customLayout.getContext(), 0 , myIntent , 0 ) ;
//                                Calendar calendar = Calendar. getInstance () ;
//                                calendar.set(Calendar. HOUR , 7 ) ;
//                                calendar.set(Calendar. AM_PM , Calendar. PM ) ;
//                                calendar.add(Calendar. DAY_OF_MONTH , 21 ) ;
//
//                            if (alarmManager != null) {
//                                alarmManager.setRepeating(AlarmManager. RTC_WAKEUP , calendar.getTimeInMillis() , 1000 * 60 * 60 * 24 , pendingIntent) ;
//                            }

                            }
                        });



                        imgCloud = customLayout.findViewById(R.id.img_weatherT);
                        Picasso.get().load(new StringBuilder("http://openweathermap.org/img/w/")
                                .append(weatherForecastResult.list.get(position).weather.get(0).getIcon())
                                .append(".png").toString()).into(imgCloud);

                        mTxtTemp = customLayout.findViewById(R.id.txt_temperatureT);
                        mTxtTemp.setText(new StringBuilder(String.valueOf(weatherForecastResult.list.get(position)
                                .main.getTemp())).append("°C"));
                        mTxtDesc = customLayout.findViewById(R.id.txt_descriptionT);
                        mTxtDesc.setText(new StringBuilder(weatherForecastResult.list.get(position)
                                .weather.get(0).getDescription()));
                        mTxtTime = customLayout.findViewById(R.id.txt_date_timeT);
                        mTxtTime.setText(new StringBuilder(Common.convertUnixToDate(weatherForecastResult
                                .list.get(position).dt)));







                    }




                }
            });



    }

    @Override
    public int getItemCount() {
        return weatherForecastResult.list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_date_time, txt_description, txt_temperature;
        ImageView img_weather;
        LinearLayout lnForecast;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            img_weather = itemView.findViewById(R.id.img_weather);
            txt_date_time = itemView.findViewById(R.id.txt_date);
            txt_description = itemView.findViewById(R.id.txt_description);
            txt_temperature = itemView.findViewById(R.id.txt_temperature);
            lnForecast = itemView.findViewById(R.id.lnForecast);
        }
    }
}
