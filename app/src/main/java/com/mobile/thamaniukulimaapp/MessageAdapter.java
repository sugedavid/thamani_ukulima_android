package com.mobile.thamaniukulimaapp;

import android.app.AlertDialog;
import android.app.Application;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.text.format.DateUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import me.himanshusoni.chatmessageview.ChatMessageView;

/**
 * Created by Lenovo on 15/09/2017.
 */

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>{

    private List<Messages> mMessageList;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase, mTodaysChatTime;
    private View mv;
    private ClipboardManager myClipboard;
    private ClipData myClip;
    public MessageAdapter(List<Messages>mMessageList){
        this.mMessageList = mMessageList;
    }



    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout, parent, false);



        return new MessageViewHolder(v);
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder{

        private TextView  timeText, timeText2, timeGeneral, timeTextImg,timeTextImg2;
        private ImageView messageImage, messageImage2;
        private TextView messageText,  messageText2;
        private CircleImageView mcircleImageView, mcircleImageView2;
        private ChatMessageView mCMV1, mCMV2, mCIMG1,mCIMG2;

        public MessageViewHolder(final View view){
            super(view);

            messageText = view.findViewById(R.id.message_text_layout);
            messageText2 = view.findViewById(R.id.message_text_layout2);
            timeText = view.findViewById(R.id.message_item_time);
            timeText2 = view.findViewById(R.id.message_item_time2);
            timeTextImg = view.findViewById(R.id.message_item_time_Img);
            timeTextImg2 = view.findViewById(R.id.message_item_time2_Img);
            timeGeneral = view.findViewById(R.id.txtGTime);
            messageImage = view.findViewById(R.id.message_image_layout);
            messageImage2 = view.findViewById(R.id.message_image_layout2);
            mcircleImageView = view.findViewById(R.id.mCprof);
            mcircleImageView2 = view.findViewById(R.id.mCprofMe);
            mCMV1 = view.findViewById(R.id.cmv1);
            mCMV2 = view.findViewById(R.id.cmv2);
            mCIMG1 = view.findViewById(R.id.cmvImg1);
            mCIMG2 = view.findViewById(R.id.cmvImg2);







        }
    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, int i) {

        mAuth = FirebaseAuth.getInstance();
        final String current_user_id = mAuth.getCurrentUser().getUid();

        final Messages c = mMessageList.get(i);

        final String from_user = c.getFrom();
        final String to_user = c.getTo();
        final String latest_cTime = c.getTime();
        final long message_time = c.getOrdertime();
        String message_type = c.getType();

        mTodaysChatTime = FirebaseDatabase.getInstance().getReference().child("Chat_Order_Time");
        mTodaysChatTime.keepSynced(true);


        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUserDatabase.keepSynced(true);
        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {



                if (dataSnapshot.exists()){

                    if (!from_user.equals(current_user_id)) {

                        final String profimage = (String) dataSnapshot.child(from_user).child("profimage").getValue();
                        Picasso.get().load(profimage).placeholder(R.drawable.userprof).networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(R.drawable.userprof)
                                .into(viewHolder.mcircleImageView);

                    }else {

                        final String profimage = (String) dataSnapshot.child(current_user_id).child("profimage").getValue();
                        Picasso.get().load(profimage).placeholder(R.drawable.userprof).networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(R.drawable.userprof)
                                .into(viewHolder.mcircleImageView2);

                    }


                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        boolean today = DateUtils.isToday(Long.parseLong(String.valueOf(message_time)) );

        GetTimeAgo getTimeAgo = new GetTimeAgo();
        long lastTime = Long.parseLong(String.valueOf(message_time));

        String ago = String.valueOf(DateUtils.getRelativeTimeSpanString(Long.parseLong(String.valueOf(message_time))));

        if (today == true){

            String simpledate = new java.text.SimpleDateFormat("hh:mm a").format(new Date(Long.parseLong(String.valueOf(message_time))));
            viewHolder.timeText.setText(simpledate);
            viewHolder.timeText2.setText(simpledate);
            viewHolder.timeTextImg.setText(simpledate);
            viewHolder.timeTextImg2.setText(simpledate);


        }else if (today == false) {



            if (ago.contains("hour")){

                String simpledate = new java.text.SimpleDateFormat("EEE,  hh:mm a").format(new Date(Long.parseLong(String.valueOf(message_time))));

                viewHolder.timeText.setText(simpledate);
                viewHolder.timeText2.setText(simpledate);
                viewHolder.timeTextImg.setText(simpledate);
                viewHolder.timeTextImg2.setText(simpledate);
            }else {

                String simpledate2 = new java.text.SimpleDateFormat("EEE, hh:mm a").format(new Date(Long.parseLong(String.valueOf(message_time))));

                viewHolder.timeText.setText(simpledate2);
                viewHolder.timeText2.setText(simpledate2);
                viewHolder.timeTextImg.setText(simpledate2);
                viewHolder.timeTextImg2.setText(simpledate2);
            }
        }


        viewHolder.mCMV1.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence options[] = new CharSequence[]{"Copy Message"};

                AlertDialog.Builder builder = new AlertDialog.Builder(viewHolder.itemView.getContext());

                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (i==0){

                            ClipboardManager clipboardManager = (ClipboardManager) viewHolder.itemView.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                            clipboardManager.setText(c.getMessage());

                            Toast.makeText(viewHolder.itemView.getContext(), "Copied to Clipboard", Toast.LENGTH_SHORT).show();
                        }else if (i==1){

                        }



                    }
                });

                builder.show();

                return true;
            }
        });




        viewHolder.mCMV2.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                CharSequence options[] = new CharSequence[]{"Copy Message"};

                AlertDialog.Builder builder = new AlertDialog.Builder(viewHolder.itemView.getContext());

                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (i==0){

                            ClipboardManager clipboardManager = (ClipboardManager) viewHolder.itemView.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                            clipboardManager.setText(c.getMessage());

                            Toast.makeText(viewHolder.itemView.getContext(), "Copied to Clipboard", Toast.LENGTH_SHORT).show();

                        }



                    }
                });

                builder.show();

                return true;
            }
        });

        viewHolder.mcircleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent profileIntent = new Intent(viewHolder.itemView.getContext(), UserInfoActivity.class);
                //profileIntent.putExtra("uid", c.getFrom());

                //viewHolder.itemView.getContext().startActivity(profileIntent);
            }
        });

        viewHolder.mcircleImageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Intent profileIntent = new Intent(viewHolder.itemView.getContext(), ProfileActivity.class);
                //viewHolder.itemView.getContext().startActivity(profileIntent);
            }
        });




        if (from_user.equals(current_user_id) && message_type.equals("text")) {


            viewHolder.messageText.setText(c.getMessage());
            viewHolder.mCMV1.setVisibility(View.VISIBLE);
            viewHolder.mCMV2.setVisibility(View.GONE);
            viewHolder.mcircleImageView.setVisibility(View.GONE);
            viewHolder.mcircleImageView2.setVisibility(View.VISIBLE);
            viewHolder.timeText.setVisibility(View.VISIBLE);
            viewHolder.timeText2.setVisibility(View.GONE);
            viewHolder.timeTextImg.setVisibility(View.GONE);
            viewHolder.timeTextImg2.setVisibility(View.GONE);
            viewHolder.messageImage.setVisibility(View.GONE);
            viewHolder.messageImage2.setVisibility(View.GONE);
            viewHolder.mCIMG2.setVisibility(View.GONE);
            viewHolder.mCIMG1.setVisibility(View.GONE);




        } else if (!from_user.equals(current_user_id) && message_type.equals("text")){

            viewHolder.messageText2.setText(c.getMessage());
            viewHolder.mCMV2.setVisibility(View.VISIBLE);
            viewHolder.mcircleImageView.setVisibility(View.VISIBLE);
            viewHolder.mcircleImageView2.setVisibility(View.GONE);
            viewHolder.mCMV1.setVisibility(View.GONE);
            viewHolder.timeText.setVisibility(View.GONE);
            viewHolder.timeText2.setVisibility(View.VISIBLE);
            viewHolder.timeTextImg.setVisibility(View.GONE);
            viewHolder.timeTextImg2.setVisibility(View.GONE);
            viewHolder.messageImage.setVisibility(View.GONE);
            viewHolder.messageImage2.setVisibility(View.GONE);
            viewHolder.mCIMG2.setVisibility(View.GONE);
            viewHolder.mCIMG1.setVisibility(View.GONE);

            //viewHolder.chatMessageView.setGravity(Gravity.LEFT);
            //viewHolder.chatMessageView.setArrowPosition(ChatMessageView.ArrowPosition.LEFT);
            //viewHolder.chatMessageView.setBackgroundColor(Color.GRAY);
            //viewHolder.messageText.setTextColor(Color.BLACK);

        }

        else if (from_user.equals(current_user_id) && message_type.equals("image")) {


            //viewHolder.messageText.setText(c.getMessage());
            viewHolder.mCMV2.setVisibility(View.GONE);
            viewHolder.mCMV1.setVisibility(View.GONE);
            viewHolder.mCIMG1.setVisibility(View.VISIBLE);
            viewHolder.mCIMG2.setVisibility(View.GONE);
            viewHolder.mcircleImageView.setVisibility(View.GONE);
            viewHolder.mcircleImageView2.setVisibility(View.VISIBLE);
            viewHolder.timeText.setVisibility(View.GONE);
            viewHolder.timeText2.setVisibility(View.GONE);
            viewHolder.timeTextImg.setVisibility(View.GONE);
            viewHolder.timeTextImg2.setVisibility(View.GONE);
            viewHolder.messageImage.setVisibility(View.VISIBLE);
            viewHolder.messageImage2.setVisibility(View.GONE);

            Picasso.get().load(c.getMessage())
                    .placeholder(R.color.darkgrayColor).into(viewHolder.messageImage);




        } else if (!from_user.equals(current_user_id) && message_type.equals("image")){

            //viewHolder.messageText2.setText(c.getMessage());
            viewHolder.mCMV2.setVisibility(View.GONE);
            viewHolder.mCMV1.setVisibility(View.GONE);
            viewHolder.mCIMG2.setVisibility(View.VISIBLE);
            viewHolder.mcircleImageView.setVisibility(View.VISIBLE);
            viewHolder.mcircleImageView2.setVisibility(View.GONE);
            viewHolder.mCIMG1.setVisibility(View.GONE);
            viewHolder.timeText.setVisibility(View.GONE);
            viewHolder.timeText2.setVisibility(View.GONE);
            viewHolder.timeTextImg.setVisibility(View.GONE);
            viewHolder.timeTextImg2.setVisibility(View.GONE);
            viewHolder.messageImage.setVisibility(View.GONE);
            viewHolder.messageImage2.setVisibility(View.VISIBLE);

            Picasso.get().load(c.getMessage())
                    .placeholder(R.color.darkgrayColor).into(viewHolder.messageImage2);


        }





        //viewHolder.timeText.setText(c.getTime());


    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


}
