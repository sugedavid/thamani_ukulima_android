package com.mobile.thamaniukulimaapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class SignUpActivity extends AppCompatActivity {

    private Button mBtnSinUp;
    private EditText mPhone;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Making notification bar transparent
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_sign_up);

        dialog = new ProgressDialog(this);
        dialog.setMessage("Finishing Setup...");
        dialog.setCancelable(false);



        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        mPhone = findViewById(R.id.phoneSU);

        mBtnSinUp = findViewById(R.id.btnSinU);

        String[] items = new String[] { "Individual", "Business"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);


        mBtnSinUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createUser();
            }
        });



    }

    private void createUser() {

        dialog.show();

        final String phone = mPhone.getText().toString().trim();

        if (phone.isEmpty()){
            dialog.dismiss();
            Toast.makeText(SignUpActivity.this,"Enter your Phone",
                    Toast.LENGTH_SHORT).show();

        }
        else if (phone.length()<9){
            dialog.dismiss();

            Toast.makeText(SignUpActivity.this,"Your Phone is too short",
                    Toast.LENGTH_SHORT).show();

        }

        else  {

            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user!=null) {

                String user_id =user.getUid();
                final DatabaseReference current_user_db = FirebaseDatabase.getInstance()
                        .getReference().child("Users").child(user_id);
                current_user_db.child("phone").setValue("+254"+phone);
                current_user_db.child("email").setValue(user.getEmail());
                current_user_db.child("username").setValue(user.getDisplayName());
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener( SignUpActivity.this,  new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        String deviceToken = instanceIdResult.getToken();
                        current_user_db.child("device_token").setValue(deviceToken).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    dialog.dismiss();
                                    startActivity(new Intent(SignUpActivity.this, MainActivity.class));
                                    finish();

                                    user.sendEmailVerification()
                                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        dialog.dismiss();
                                                        Toast.makeText(SignUpActivity.this, "Email verification sent",
                                                                Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        dialog.dismiss();
                                                        //startActivity(new Intent(SignUpActivity.this, PhoneCreateActivity.class));
                                                        // If sign in fails, display a message to the user.
                                                        Toast.makeText(SignUpActivity.this, "Could not Send email verification",
                                                                Toast.LENGTH_SHORT).show();
                                                    }
                                                }
                                            });
                                } else {
                                    Toast.makeText(SignUpActivity.this, "Something went wrong! Try Again", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                    }
                });

            }

        }
    }



}

