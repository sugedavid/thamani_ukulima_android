package com.mobile.thamaniukulimaapp;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AdminActivity extends AppCompatActivity {

    private EditText mEdtCrop, mEdtActivity, mEdtCondition1, mEdtCondition2, mEdtDuration;
    private Button mBtnSave;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase;
    private String userID;
    private ProgressBar loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_admin);

        mEdtCrop = findViewById(R.id.edtCropFA);
        mEdtActivity = findViewById(R.id.edtActivityFA);
        mEdtCondition1 = findViewById(R.id.edtCondition1);
        mEdtCondition2 = findViewById(R.id.edtCondition2);
        mEdtDuration = findViewById(R.id.edtDurationFA);
        mBtnSave = findViewById(R.id.btnSaveFA);
        loading = findViewById(R.id.loadingFA);

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("FarmingActivity");
        mUserDatabase.keepSynced(true);

        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveActivities();
            }
        });




    }

    private void saveActivities() {

        loading.setVisibility(View.VISIBLE);
        String crop = mEdtCrop.getText().toString().trim();
        String activity = mEdtActivity.getText().toString().trim();
        String condition1 = mEdtCondition1.getText().toString().trim();
        String condition2 = mEdtCondition2.getText().toString().trim();
        String duration = mEdtDuration.getText().toString().trim();

        if (!crop.isEmpty() && !activity.isEmpty() && !condition1.isEmpty() && !condition2.isEmpty() &&
                !duration.isEmpty()){

            mUserDatabase.child(crop).child(activity).child("Condition").child("1").setValue(condition1);
            mUserDatabase.child(crop).child(activity).child("Condition").child("2").setValue(condition2);
            mUserDatabase.child(crop).child(activity).child("Duration").setValue(duration)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        loading.setVisibility(View.GONE);
                        Toast.makeText(AdminActivity.this, "Saved Successfully", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }else {
            loading.setVisibility(View.GONE);
            Toast.makeText(this, "Fill in all details", Toast.LENGTH_SHORT).show();
        }

    }
}
