package com.mobile.thamaniukulimaapp.FirebaseModel;

public class ForumModel {

    private String userID;
    private String forumText;
    private long time;

    public ForumModel(){

    }

    public ForumModel(String userID, String forumText, long time) {
        this.userID = userID;
        this.forumText = forumText;
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getForumText() {
        return forumText;
    }

    public void setForumText(String forumText) {
        this.forumText = forumText;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }
}
