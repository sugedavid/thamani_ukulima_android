package com.mobile.thamaniukulimaapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Arrays;
import java.util.List;

public class AddFarmActivity extends AppCompatActivity {

    private LinearLayout mLnSearchLoc;
    private EditText mEdtFarrmDesc;
    private TextView mTxtSearchLoc;
    private Button mBtnSaveLoc;
    private LatLng farmLocation;
    int AUTOCOMPLETE_REQUEST_CODE = 1;
    PlacesClient placesClient;
    List<Place.Field> placeFields = Arrays.asList(Place.Field.ID,
            Place.Field.LAT_LNG,
            Place.Field.NAME,
            Place.Field.ADDRESS);

    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase, mCustomerDatabase,mDtbUser;
    private String userID, placeName;
    private ProgressBar loading;
    private String status;
    private String crop, farmID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setElevation(0);
        Places.initialize(AddFarmActivity.this, getString(R.string.places_key));
        placesClient = Places.createClient(AddFarmActivity.this);
        setContentView(R.layout.activity_add_farm);



        // Set the fields to specify which types of place data to
// return after the user has made a selection.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG);

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        mDtbUser = FirebaseDatabase.getInstance()
                .getReference().child("Users").child(userID);
        mDtbUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("MyFarms").exists()) {
                    status = "exists";
                } else {
                    status = "empty";
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                .child("MyFarms").push();
        mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);
        mCustomerDatabase.keepSynced(true);

        farmID = mUserDatabase.getKey();


        mBtnSaveLoc = findViewById(R.id.btnSaveLoc);
        mEdtFarrmDesc = findViewById(R.id.edtFarmDesc);
        mTxtSearchLoc = findViewById(R.id.txtSearchLoc);
        mLnSearchLoc = findViewById(R.id.lnSearchLoc);
        loading = findViewById(R.id.loadingAddLoc);

        mBtnSaveLoc.setVisibility(View.INVISIBLE);

        mTxtSearchLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Start the autocomplete intent.
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.OVERLAY, fields)
                        .setCountry("KE")
                        .build(AddFarmActivity.this);
                startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);

            }
        });

        final Spinner dynamicSpinner = findViewById(R.id.dynamic_spinner);

        String[] items = new String[] {"Maize"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, items);

        dynamicSpinner.setAdapter(adapter);

        dynamicSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                  crop = parent.getItemAtPosition(position).toString();
                  mBtnSaveLoc.setVisibility(View.VISIBLE);
//                mDatabaseUser.child("paymentmethodvalue").setValue(String.valueOf(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        
        mBtnSaveLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
                saveFarm();
            }
        });
    }

    private void saveFarm() {

        loading.setVisibility(View.VISIBLE);

        String desc = mEdtFarrmDesc.getText().toString().trim();


        if (farmLocation == null){
            loading.setVisibility(View.GONE);
            Toast.makeText(this, "Enter Location", Toast.LENGTH_SHORT).show();
        }
        else if (desc.isEmpty()){
            loading.setVisibility(View.GONE);
            Toast.makeText(this, "Enter Description", Toast.LENGTH_SHORT).show();
        }
        else if (placeName.isEmpty()){
            placeName = desc;
        }
        else if (crop.isEmpty()){
            Toast.makeText(this, "Select Crop Grown", Toast.LENGTH_SHORT).show();
        }
        else  {
            mUserDatabase.child("Crop").setValue(crop);
            mUserDatabase.child("Longitude").setValue(farmLocation.longitude);
            mUserDatabase.child("Latitude").setValue(farmLocation.latitude);
            //mUserDatabase.child("Latlng").setValue(farmLocation);
            mUserDatabase.child("PlaceName").setValue(placeName);
            mUserDatabase.child("Description").setValue(desc).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if (task.isSuccessful()){
                        Toast.makeText(AddFarmActivity.this, "Farm Saved", Toast.LENGTH_SHORT).show();
                        loading.setVisibility(View.GONE);
                        if (status.equals("empty")){
                            mCustomerDatabase.child("Favorite").child("Latitude").setValue(farmLocation.latitude);
                            mCustomerDatabase.child("Favorite").child("Longitude").setValue(farmLocation.longitude);
                            mCustomerDatabase.child("Favorite").child("Crop").setValue(crop);
                            mCustomerDatabase.child("Favorite").child("FarmID").setValue(farmID).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        startActivity(new Intent(AddFarmActivity.this, MainActivity.class));
                                        finish();
                                    }
                                }
                            });

                        }else {
                            finish();
                        }

                    }
                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                farmLocation = place.getLatLng();
                placeName = place.getName();
                mTxtSearchLoc.setText(place.getName());

                //Toast.makeText(this, "Place: " + place.getName(), Toast.LENGTH_SHORT).show();
                //Log.i(TAG, "Place: " + place.getName() + ", " + place.getId());
            } else if (resultCode == RESULT_CANCELED) {
                //mEdtFarrmDesc.setText("");
                // The user canceled the operation.
            }
        }
    }
}
