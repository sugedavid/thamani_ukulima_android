package com.mobile.thamaniukulimaapp.MainFragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.mobile.thamaniukulimaapp.Adapter.WeatherForecastAdapterB;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.Model.WeatherForecastResult;
import com.mobile.thamaniukulimaapp.Model.WeatherResult;
import com.mobile.thamaniukulimaapp.R;
import com.mobile.thamaniukulimaapp.Retrofit.IOpenWeatherMap;
import com.mobile.thamaniukulimaapp.Retrofit.RetrofitClient;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class WeatherInfoFragment extends Fragment {

    private static WeatherInfoFragment instance;
    private TextView txt_city_name, txt_humidity, txt_sunrise, txt_sunset, txt_pressure, txt_description, txt_date_time,
            txt_wind, txt_temperature,txt_geo_cord;
    private ImageView img_weather;
    private LinearLayout weather_panel;
    private ProgressBar loading;

    private CompositeDisposable compositeDisposable;
    private IOpenWeatherMap mService;

    private RecyclerView recycler_forecast;

    private LocationCallback locationCallback;
    private LocationRequest locationRequest;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private FirebaseAuth mAuth;
    private String userID, farmID, crop;
    private double dbLat, dbLng;

    private TextView mTxtTitle, mTxtFarmCrop;
    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;

    private DatabaseReference mUserDatabase;


    public static WeatherInfoFragment getInstance(){
        if (instance == null)
            instance = new WeatherInfoFragment();
        return instance;
    }


    public WeatherInfoFragment() {
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getInstance();
        mService = retrofit.create(IOpenWeatherMap.class);
    }





    @SuppressLint("WrongConstant")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View itemView = inflater.inflate(R.layout.fragment_weather_info, container, false);

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnMap  = getActivity().findViewById(R.id.imgBtnMap);
        mImgProf = getActivity().findViewById(R.id.imgProf);
        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);
        mTxtTitle.setText("Weather Info");
        imgBtnBack.setVisibility(View.VISIBLE);
        imgBtnMap.setVisibility(View.GONE);
        mImgProf.setVisibility(View.GONE);
        imgBtnSort.setVisibility(View.GONE);


        recycler_forecast = itemView.findViewById(R.id.recycler_forecast);
        recycler_forecast.setHasFixedSize(true);
        recycler_forecast.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));


        mTxtFarmCrop = itemView.findViewById(R.id.txt_farm_crop);


        img_weather = itemView.findViewById(R.id.img_weather);
        txt_city_name = itemView.findViewById(R.id.txt_city_name);
        txt_date_time = itemView.findViewById(R.id.txt_date_time);
        txt_description = itemView.findViewById(R.id.txt_description);
        txt_geo_cord = itemView.findViewById(R.id.txt_geo_cood);
        txt_humidity = itemView.findViewById(R.id.txt_humidity);
        txt_pressure = itemView.findViewById(R.id.txt_pressure);
        txt_sunrise = itemView.findViewById(R.id.txt_sunrise);
        txt_sunset = itemView.findViewById(R.id.txt_sunset);
        txt_wind = itemView.findViewById(R.id.txt_wind);
        txt_temperature = itemView.findViewById(R.id.txt_temperature);
        weather_panel = itemView.findViewById(R.id.weather_panel);
        loading = itemView.findViewById(R.id.loading);

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();


        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {


                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                                return;
                            }

                            getWeatherInformation();
                            getForecastWeatherInformation();

                        }

                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {

                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Permission Denied", Snackbar.LENGTH_LONG).show();
                    }
                }).check();



        return itemView;
    }

    private void getForecastWeatherInformation() {
        if (getArguments() != null) {
            dbLat = getArguments().getDouble("lat");
            dbLng = getArguments().getDouble("lng");
            farmID = getArguments().getString("farmID");
            crop = getArguments().getString("crop");
            compositeDisposable.add(mService.getForecastWeatherByLatLng(
                    String.valueOf(dbLat),
                    String.valueOf(dbLng),
                    Common.API_KEY,
                    "metric")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<WeatherForecastResult>() {
                        @Override
                        public void accept(WeatherForecastResult weatherForecastResult) throws Exception {
                            displayForecastWeather(weatherForecastResult, farmID, crop);




                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Snackbar.make(getActivity().findViewById(android.R.id.content), throwable.getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    })


            );

            mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                    .child("MyFarms").child(farmID);
            mUserDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                    String crop = (String) dataSnapshot.child("Crop").getValue();
                    if (crop!=null){
                        mTxtFarmCrop.setText(crop);
                    }else {
                        mTxtFarmCrop.setText("None");
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    private void displayForecastWeather(WeatherForecastResult weatherForecastResult, String fID, String crop) {

        WeatherForecastAdapterB adapter = new WeatherForecastAdapterB(getContext(),weatherForecastResult, fID, crop);

        recycler_forecast.setAdapter(adapter);
    }

    private void getWeatherInformation() {

        if (getArguments() != null) {
            dbLat = getArguments().getDouble("lat");
            dbLng = getArguments().getDouble("lng");
            compositeDisposable.add(mService.getWeatherByLatLng(
                    String.valueOf(dbLat),
                    String.valueOf(dbLng),
                    Common.API_KEY,
                    "metric")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<WeatherResult>() {
                        @Override
                        public void accept(WeatherResult weatherResult) throws Exception {
                            //Load Image
                            Picasso.get().load(new StringBuilder("http://openweathermap.org/img/w/")
                                    .append(weatherResult.getWeather().get(0).getIcon())
                                    .append(".png").toString()).into(img_weather);

                            txt_city_name.setText(weatherResult.getName());
                            /*txt_description.setText(new StringBuilder("Weather in ")
                                    .append(weatherResult.getName()).toString());*/
                            txt_temperature.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getTemp()))
                                    .append("°C").toString());
                            txt_date_time.setText("Updated "+Common.convertUnixToDate(weatherResult.getDt()));
                            txt_pressure.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getPressure()))
                                    .append(" hpa").toString());
                            txt_humidity.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getHumidity()))
                                    .append(" %").toString());
                            txt_sunrise.setText(Common.convertUnixToHour((long) weatherResult.getSys().getSunrise()));
                            txt_sunset.setText(Common.convertUnixToHour((long) weatherResult.getSys().getSunset()));
                            txt_geo_cord.setText(new StringBuilder("[")
                                    .append(weatherResult.getCoord().toString())
                                    .append("]").toString());
                            weather_panel.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            loading.setVisibility(View.GONE);
                            Snackbar.make(getActivity().findViewById(android.R.id.content), throwable.getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    }));
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mTxtTitle.setText("Home");
        //imgBtnMap.setVisibility(View.VISIBLE);
        mImgProf.setVisibility(View.VISIBLE);
        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnBack.setVisibility(View.GONE);
    }
}