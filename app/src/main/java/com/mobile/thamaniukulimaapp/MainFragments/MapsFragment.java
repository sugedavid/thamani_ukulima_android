package com.mobile.thamaniukulimaapp.MainFragments;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Looper;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.thamaniukulimaapp.R;

import java.util.ArrayList;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.firebase.ui.auth.AuthUI.getApplicationContext;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FirebaseAuth mAuth;
    private DatabaseReference mUserDatabase, mCustomerDatabase;
    private String userID, placeName, farmID, crop;
    private View mView;
    private TextView mTxtTitle, mTxtFarmCrop;
    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;


    public MapsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_maps, container, false);

        //mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnMap  = getActivity().findViewById(R.id.imgBtnMap);
        mImgProf = getActivity().findViewById(R.id.imgProf);
        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);
        mTxtTitle.setText("Map");
        imgBtnBack.setVisibility(View.VISIBLE);
        imgBtnMap.setVisibility(View.GONE);
        mImgProf.setVisibility(View.GONE);
        imgBtnSort.setVisibility(View.GONE);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                .child("MyFarms");
        mUserDatabase.keepSynced(true);
        mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);
        mCustomerDatabase.keepSynced(true);
        mCustomerDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                farmID = (String) dataSnapshot.child("Favorite").child("FarmID").getValue();
                crop = (String) dataSnapshot.child("Favorite").child("Crop").getValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        return mView;
    }

    private void collectPhoneNumbers(Map<String,Object> users) {

//        int size = (int) dataSnapshot.getChildrenCount(); //
//        Marker[] allMarkers = new Marker[size];
//        Marker mm;

        ArrayList<Double> phoneNumbers = new ArrayList<>();
        ArrayList<Double> latitude = new ArrayList<>();
        double lat, lng;
        String desc;

        //iterate through each user, ignoring their UID
        for (Map.Entry<String, Object> entry : users.entrySet()){

            //Get user map
            Map singleUser = (Map) entry.getValue();
            //Get phone field and append to list
            phoneNumbers.add((Double) singleUser.get("Longitude"));
            latitude.add((Double) singleUser.get("Latitude"));
            lat = (double) singleUser.get("Latitude");
            lng = (double) singleUser.get("Longitude");
            desc = (String) singleUser.get("PlaceName");
            LatLng latLng = new LatLng(lat, lng);
            //Toast.makeText(getContext(), ""+ latLng  , Toast.LENGTH_SHORT).show();
            if (mMap!=null) {
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, lng))
                        .title(desc));
                //mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,6));
            }
//            mMap.addMarker(new MarkerOptions()
//                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)).position(latLng).title("Farm"));

        }

        //System.out.println(phoneNumbers.toString());
        //Toast.makeText(getContext(), phoneNumbers.toString(), Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {


        mMap = googleMap;
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){
                    collectPhoneNumbers((Map<String,Object>) dataSnapshot.getValue());
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            double lat = marker.getPosition().latitude;
                            double lng = marker.getPosition().longitude;
                            Bundle bundle = new Bundle();
                            bundle.putDouble("lat", lat);
                            bundle.putDouble("lng", lng);
                            bundle.putString("farmID", farmID);
                            bundle.putString("crop", crop);
                            WeatherInfoFragment weatherInfoFragment = new WeatherInfoFragment();
                            weatherInfoFragment.setArguments(bundle);

                            getParentFragmentManager().beginTransaction().add(R.id.frameLayout, weatherInfoFragment).commit();

                        }
                    });

                }else {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Add new location to view on map", Snackbar.LENGTH_LONG).show();

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        try {
            boolean isSuccess = googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getContext(),
                    R.raw.custom_style_map));

            if (!isSuccess){
                Toast.makeText(getContext(), "Could not load Map Style", Toast.LENGTH_SHORT).show();

            }
        }

        catch (Resources.NotFoundException ex){

            ex.printStackTrace();
        }




    }

    private void checkLocationPermission() {
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(getContext())
                        .setTitle("Give permission")
                        .setMessage("Allow Vibarua to access your device's location")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                            }
                        })
                        .create()
                        .show();
            }
            else{
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    //mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
                    mMap.setMyLocationEnabled(true);

                }
            } else {
                Toast.makeText(getContext(), "Please provide the permission", Toast.LENGTH_LONG).show();
            }
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        mTxtTitle.setText("Home");
        imgBtnMap.setVisibility(View.VISIBLE);
        mImgProf.setVisibility(View.VISIBLE);
        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnBack.setVisibility(View.GONE);
    }
}
