package com.mobile.thamaniukulimaapp.MainFragments;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobile.thamaniukulimaapp.CreditsActivity;
import com.mobile.thamaniukulimaapp.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private View mview;

    private EditText edtUsername;
    private TextView mEmailField, mPhoneField;
    private ImageButton imgBtnSaveProf;
    private CardView mCdvCredits;

    private CircleImageView mProfileImage;

    private FirebaseAuth mAuth;
    private DatabaseReference mCustomerDatabase;

    private String userID;
    private String mName;
    private String mPhone,mUsername,mEmail;
    private String mProfileImageUrl;
    private Uri resultUri;
    private ProgressDialog dialog;
    private static final int REQUEST_READ_EXTERNAL = 1234;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_profile, container, false);

        mPhoneField = mview.findViewById(R.id.phone);
        mEmailField = mview.findViewById(R.id.email);
        edtUsername = mview.findViewById(R.id.usernameP);
        imgBtnSaveProf = mview.findViewById(R.id.imgBtnSaveProf);
        mCdvCredits = mview.findViewById(R.id.cdvCredits);

        mProfileImage = mview.findViewById(R.id.profileImage);

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Updating info...");

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            String email = user.getEmail();
            String phone = user.getPhoneNumber();
            Uri photoUrl = user.getPhotoUrl();
            if (user.getPhoneNumber()!=null) {
                mPhoneField.setText(phone);
            }
            if (user.getDisplayName()!=null) {
                edtUsername.setText(name);
            }
            if (user.getPhotoUrl()!=null) {
                mProfileImage.setImageURI(photoUrl);
            }
            // Check if user's email is verified
            boolean emailVerified = user.isEmailVerified();

            String uid = user.getUid();
        }

        mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);

        getUserInfo();

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermissions();
            }
        });

        imgBtnSaveProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserInformation();
            }
        });

        mCdvCredits.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), CreditsActivity.class));
            }
        });



        return mview;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_READ_EXTERNAL) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);


            } else {

                Snackbar.make(getActivity().findViewById(android.R.id.content), "Please grant read permission to update your profile image", Snackbar.LENGTH_SHORT).show();

            }
        }
    }


    private void checkPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){

            if ((ContextCompat.checkSelfPermission(getContext(), READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) ){

                // Permission is not granted


                ActivityCompat.requestPermissions(getActivity(), new String[]{READ_EXTERNAL_STORAGE }, REQUEST_READ_EXTERNAL);



            }else {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);

            }



        }else {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, 1);
        }
    }


    private void getUserInfo(){
        mCustomerDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();

                    if(map.get("email")!=null){
                        mEmail = map.get("email").toString();
                        mEmailField.setText(mEmail);
                        //mEmailField.setText(mName);
                    }else {
                        mEmailField.setText("Add Email");
                    }
//                    if(map.get("phone")!=null){
//                        mPhone = map.get("phone").toString();
//                        mPhoneField.setText(mPhone);
//                    }

                    if(map.get("username")!=null){
                        mUsername = map.get("username").toString();
                        edtUsername.setText(mUsername);
                    }
                    else {
                        edtUsername.setText("Username");
                    }
                    if(map.get("profileImageUrl")!=null){
                        mProfileImageUrl = (String) map.get("profileImageUrl");

                        Picasso.get().load(mProfileImageUrl).networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(R.drawable.userprof).into(mProfileImage, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {

                                Picasso.get().load(mProfileImageUrl).placeholder(R.drawable.userprof).into(mProfileImage);

                            }



                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }



    private void saveUserInformation() {

        dialog.show();
        mPhone = mAuth.getCurrentUser().getPhoneNumber();
        mUsername = edtUsername.getText().toString();
        mEmail = mEmailField.getText().toString();



        if (mUsername.isEmpty()){
            dialog.dismiss();
            Toast.makeText(getContext(), "Empty Username", Toast.LENGTH_SHORT).show();
        }
        else if (mPhone == null){
            dialog.dismiss();
            Toast.makeText(getContext(), "No Phone Registered", Toast.LENGTH_SHORT).show();
        }
        else if(mUsername.length()<3){
            dialog.dismiss();

            Toast.makeText(getContext(),"Your Username is too short",
                    Toast.LENGTH_SHORT).show();

        }
        else if(!mEmail.isEmpty() && !mEmail.contains("@")){
            dialog.dismiss();

            Toast.makeText(getContext(),"Invalid email",
                    Toast.LENGTH_SHORT).show();

        }
        else {

            Map userInfo = new HashMap();

            userInfo.put("phone", mPhone);
            userInfo.put("username", mUsername);
            userInfo.put("email", mEmail);
            mCustomerDatabase.updateChildren(userInfo).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()){
                        dialog.dismiss();
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Updated", Snackbar.LENGTH_SHORT).show();

                    }else {
                        dialog.dismiss();
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Profile Update Unsuccessful" + task.getException(), Snackbar.LENGTH_SHORT).show();
                    }

                }
            });


            if (resultUri != null) {
                dialog.show();
                dialog.setTitle("Updating profile pic...");
                final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("profile_images").child(userID);
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), resultUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                byte[] data = baos.toByteArray();
                final UploadTask uploadTask = filePath.putBytes(data);


                uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //Uri url = taskSnapshot.getDownloadUrl();

                        final Map newImage = new HashMap();

                        filePath.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {

                                newImage.put("profileImageUrl", task.getResult().toString());
                                mCustomerDatabase.updateChildren(newImage);
                                dialog.dismiss();

                                Snackbar.make(getActivity().findViewById(android.R.id.content), "Profile photo updated", Snackbar.LENGTH_SHORT).show();


                            }
                        });


                    }
                });
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: "+ e.getMessage(), Snackbar.LENGTH_SHORT).show();

                    }
                });
            }

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            final Uri imageUri = data.getData();
            resultUri = imageUri;
            mProfileImage.setImageURI(resultUri);
        }
    }

}
