package com.mobile.thamaniukulimaapp.MainFragments;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.ProfileActivity;
import com.mobile.thamaniukulimaapp.R;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreatePostFragment extends Fragment {

    private View mView;
    private TextView mTxtTitle;
    private ImageButton imgBtnBack, mImgPost, imgBtnSort;
    private CircleImageView mImgProf;
    private Button mBtnPost;
    private EditText mEdtPost;
    private String stForum;
    private Uri resultUri;

    private DatabaseReference mDtbPostForum;
    private StorageReference filePath;
    private String userID;
    private ProgressBar pbCreatePost;

    public CreatePostFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_create_post, container, false);

        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);
        mImgProf = getActivity().findViewById(R.id.imgProf);

        imgBtnSort.setVisibility(View.GONE);
        mImgProf.setVisibility(View.GONE);

        mEdtPost  = mView.findViewById(R.id.edtPostForum);
        mImgPost  = mView.findViewById(R.id.imgPostForum);
        mBtnPost  = mView.findViewById(R.id.btnPostForum);
        pbCreatePost  = mView.findViewById(R.id.pbCreatePost);

        userID = Common.getUserID();
        mDtbPostForum = FirebaseDatabase.getInstance().getReference().child("Forums").push();
        filePath = FirebaseStorage.getInstance().getReference().child("forum_images").child(userID);

        mImgPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission();
            }
        });
        
        mBtnPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postToForum();
            }
        });

        return mView;
    }

    private void checkPermission() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        Intent intent = new Intent(Intent.ACTION_PICK);
                        intent.setType("image/*");
                        startActivityForResult(intent, 1);
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void postToForum() {
        mBtnPost.setEnabled(false);
        pbCreatePost.setVisibility(View.VISIBLE);
        stForum = mEdtPost.getText().toString().trim();

        // Nothing
        if (stForum.isEmpty() && resultUri == null){

            pbCreatePost.setVisibility(View.GONE);
            Snackbar.make(getActivity().findViewById(android.R.id.content), "Share something", Snackbar.LENGTH_SHORT).show();
            mBtnPost.setEnabled(true);

        }
        // Text Only
        else if (!stForum.isEmpty() && resultUri == null){

            Map postMap = new HashMap();
            postMap.put("forumText", stForum);
            postMap.put("userID", userID);
            postMap.put("time", ServerValue.TIMESTAMP);
            mDtbPostForum.updateChildren(postMap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if (task.isSuccessful()){
                        AppCompatActivity activity = (AppCompatActivity) mView.getContext();
                        activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        Snackbar.make(getActivity().findViewById(android.R.id.content), "Posted", Snackbar.LENGTH_SHORT).show();
                        mBtnPost.setEnabled(true);
                    }
                }
            });

            // Image Only
        }else if (stForum.isEmpty() && resultUri != null){

            Map postMap = new HashMap();
            postMap.put("userID", userID);
            postMap.put("time", ServerValue.TIMESTAMP);

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
            byte[] data = baos.toByteArray();
            final UploadTask uploadTask = filePath.putBytes(data);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //Uri url = taskSnapshot.getDownloadUrl();

                    filePath.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {

                            postMap.put("forumImageUrl", task.getResult().toString());
                            mDtbPostForum.updateChildren(postMap);

                            AppCompatActivity activity = (AppCompatActivity) mView.getContext();
                            activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            Snackbar.make(getActivity().findViewById(android.R.id.content), "Posted", Snackbar.LENGTH_SHORT).show();
                            mBtnPost.setEnabled(true);
                        }
                    });

                }
            });
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: "+ e.getMessage(), Snackbar.LENGTH_SHORT).show();
                    mBtnPost.setEnabled(true);
                }
            });
        }
        // Image & Text
        else {

            Map postMap = new HashMap();
            postMap.put("forumText", stForum);
            postMap.put("userID", userID);
            postMap.put("time", ServerValue.TIMESTAMP);

            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), resultUri);
            } catch (IOException e) {
                e.printStackTrace();
            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 20, baos);
            byte[] data = baos.toByteArray();
            final UploadTask uploadTask = filePath.putBytes(data);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //Uri url = taskSnapshot.getDownloadUrl();

                    filePath.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {

                            postMap.put("forumImageUrl", task.getResult().toString());
                            mDtbPostForum.updateChildren(postMap);

                            AppCompatActivity activity = (AppCompatActivity) mView.getContext();
                            activity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            Snackbar.make(getActivity().findViewById(android.R.id.content), "Posted", Snackbar.LENGTH_SHORT).show();
                            mBtnPost.setEnabled(true);
                        }
                    });

                }
            });
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "Error: "+ e.getMessage(), Snackbar.LENGTH_SHORT).show();
                    mBtnPost.setEnabled(true);
                }
            });

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK){
            final Uri imageUri = data.getData();
            resultUri = imageUri;
            mImgPost.setImageURI(resultUri);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTxtTitle.setText("Forum");
        imgBtnBack.setVisibility(View.GONE);
        imgBtnSort.setVisibility(View.GONE);
        mImgProf.setVisibility(View.VISIBLE);
    }
}
