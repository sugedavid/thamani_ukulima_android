package com.mobile.thamaniukulimaapp.MainFragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;
import com.mobile.thamaniukulimaapp.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecordsFragment extends Fragment {

    View mview;
    private ViewPager viewPager;
    private TabLayout tabs;



    public RecordsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_records, container, false);

        //sectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());
        viewPager = mview.findViewById(R.id.view_pagerRecords);
        setupViewPager(viewPager);
        //viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setCurrentItem(0);

        tabs = mview.findViewById(R.id.tabsRecords);
        tabs.setupWithViewPager(viewPager);

        return mview;
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {


        Adapter adapter = new Adapter(getChildFragmentManager());
        //adapter.addFragment(new SalesFragment(), "Sales");
        //adapter.addFragment(new InventoryFragment(), "Inventory");
        viewPager.setAdapter(adapter);



    }

    public class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }



}
