package com.mobile.thamaniukulimaapp.MainFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FarmTimelineFragment extends Fragment {

    private View mView;
    private TextView mTxtTitle, mTxtFarmCrop;
    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;
    private String lp_desc, key;
    private Long lps_time, lpe_time;
    private TextView mtxtLPS_time, mtxtLPE_time, mtxtLP_desc, mtxtSS_time_start, mtxtSS_time_end, mtxtSS_desc,
            mtxtP_time_start, mtxtP_time_end, mtxtP_desc, mtxtF_time_start, mtxtF_time_end, mtxtF_desc,
            mtxtW_time_start, mtxtW_time_end, mtxtW_desc, mtxtH_time_start, mtxtH_time_end, mtxtH_desc;
    private CheckBox checkBox_land_prep, checkBox_seeds, checkBox_plant, checkBox_fertilizer, checkBox_weed, checkBox_harvest;
    private DatabaseReference mTaskDB;
    private String userID = Common.getUserID();

    public FarmTimelineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_farm_timeline, container, false);

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnMap  = getActivity().findViewById(R.id.imgBtnMap);
        mImgProf = getActivity().findViewById(R.id.imgProf);
        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);
        mTxtTitle.setText("Farm Timeline");
        imgBtnBack.setVisibility(View.VISIBLE);
        imgBtnMap.setVisibility(View.GONE);
        mImgProf.setVisibility(View.GONE);
        imgBtnSort.setVisibility(View.GONE);

        // ------- LAND PREP ---------
        mtxtLPS_time  = mView.findViewById(R.id.txtLP_time_start);
        mtxtLPE_time  = mView.findViewById(R.id.txtLP_time_end);
        mtxtLP_desc  = mView.findViewById(R.id.txtLP_desc);
        // ------- SEED SELECTION ---------
        mtxtSS_time_start  = mView.findViewById(R.id.txtSS_time_start);
        mtxtSS_time_end  = mView.findViewById(R.id.txtSS_time_end);
        mtxtSS_desc  = mView.findViewById(R.id.txtSS_desc);
        // ------- PLANT ---------
        mtxtP_time_start  = mView.findViewById(R.id.txtP_time_start);
        mtxtP_time_end  = mView.findViewById(R.id.txtP_time_end);
        mtxtP_desc  = mView.findViewById(R.id.txtP_desc);
        // ------- FERTILIZER ---------
        mtxtF_time_start  = mView.findViewById(R.id.txtF_time_start);
        mtxtF_time_end  = mView.findViewById(R.id.txtF_time_end);
        mtxtF_desc  = mView.findViewById(R.id.txtF_desc);
        // ------- WEED ---------
        mtxtW_time_start  = mView.findViewById(R.id.txtW_time_start);
        mtxtW_time_end = mView.findViewById(R.id.txtW_time_end);
        mtxtW_desc = mView.findViewById(R.id.txtW_desc);
        // ------- HARVEST ---------
        mtxtH_time_start  = mView.findViewById(R.id.txtH_time_start);
        mtxtH_time_end = mView.findViewById(R.id.txtH_time_end);
        mtxtH_desc = mView.findViewById(R.id.txtH_desc);

        checkBox_land_prep = mView.findViewById(R.id.check_land_Prep);
        checkBox_seeds = mView.findViewById(R.id.check_seeds);
        checkBox_plant = mView.findViewById(R.id.check_plant);
        checkBox_fertilizer = mView.findViewById(R.id.check_fertilizer);
        checkBox_weed = mView.findViewById(R.id.check_weed);
        checkBox_harvest = mView.findViewById(R.id.check_harvest);

        key = getArguments().getString("key");

        mTaskDB = FirebaseDatabase.getInstance().getReference().child("Tasks").child(userID);
        mTaskDB.keepSynced(true);

        return  mView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mTaskDB.child(key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                final String lp_status = (String) dataSnapshot.child("lp_status").child("status").getValue();
                final String s_status = (String) dataSnapshot.child("s_status").child("status").getValue();
                final String p_status = (String) dataSnapshot.child("p_status").child("status").getValue();
                final String f_status = (String) dataSnapshot.child("f_status").child("status").getValue();
                final String w_status = (String) dataSnapshot.child("w_status").child("status").getValue();
                final String h_status = (String) dataSnapshot.child("h_status").child("status").getValue();
                String ls, le, se, pe, fe, we;

                mtxtLP_desc.setText("Land Prep");
                mtxtSS_desc.setText("Seed Selection");
                mtxtP_desc.setText("Plant");
                mtxtF_desc.setText("Fertilizer");
                mtxtW_desc.setText("Weed");
                mtxtH_desc.setText("Harvest");

                // ---- LAND PREP --------
                if (dataSnapshot.child("ls").exists()) {
                    mtxtLPS_time.setText(Common.getTime((long) dataSnapshot.child("ls").getValue()));
                }
                if (dataSnapshot.child("le").exists()) {
                    mtxtLPE_time.setText(Common.getTime((long) dataSnapshot.child("le").getValue()));
                }
                // ---- SEED SELECTION --------
                if (dataSnapshot.child("le").exists()) {
                    mtxtSS_time_start.setText(Common.getTime((long) dataSnapshot.child("le").getValue()));
                }
                if (dataSnapshot.child("se").exists()) {
                    mtxtSS_time_end.setText(Common.getTime((long) dataSnapshot.child("se").getValue()));
                }
                // ---- PLANT --------
                if (dataSnapshot.child("se").exists()) {
                    mtxtP_time_start.setText(Common.getTime((long) dataSnapshot.child("se").getValue()));
                }
                if (dataSnapshot.child("pe").exists()) {
                    mtxtP_time_end.setText(Common.getTime((long) dataSnapshot.child("pe").getValue()));
                }
                // ---- FERTILIZER --------
                if (dataSnapshot.child("pe").exists()) {
                    mtxtF_time_start.setText(Common.getTime((long) dataSnapshot.child("pe").getValue()));
                }
                if (dataSnapshot.child("fe").exists()) {
                    mtxtF_time_end.setText(Common.getTime((long) dataSnapshot.child("fe").getValue()));
                }
                // ---- WEED --------
                if (dataSnapshot.child("fe").exists()) {
                    mtxtW_time_start.setText(Common.getTime((long) dataSnapshot.child("fe").getValue()));
                }
                if (dataSnapshot.child("we").exists()) {
                    mtxtW_time_end.setText(Common.getTime((long) dataSnapshot.child("we").getValue()));
                }
                // ---- HARVEST --------
                if (dataSnapshot.child("we").exists()) {
                    mtxtH_time_start.setText(Common.getTime((long) dataSnapshot.child("we").getValue()));
                }
                if (dataSnapshot.child("he").exists()) {
                    mtxtH_time_end.setText(Common.getTime((long) dataSnapshot.child("he").getValue()));
                }

                checkBox_land_prep.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            mTaskDB.child(key).child("lp_status").child("status").setValue("Complete");
                        }else {
                            mTaskDB.child(key).child("lp_status").child("status").setValue("Incomplete");
                        }
                    }
                });
                checkBox_seeds.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            mTaskDB.child(key).child("s_status").child("status").setValue("Complete");
                        }else {
                            mTaskDB.child(key).child("s_status").child("status").setValue("Incomplete");
                        }
                    }
                });
                checkBox_plant.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            mTaskDB.child(key).child("p_status").child("status").setValue("Complete");
                        }else {
                            mTaskDB.child(key).child("p_status").child("status").setValue("Incomplete");
                        }
                    }
                });
                checkBox_fertilizer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            mTaskDB.child(key).child("f_status").child("status").setValue("Complete");
                        }else {
                            mTaskDB.child(key).child("f_status").child("status").setValue("Incomplete");
                        }
                    }
                });
                checkBox_weed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            mTaskDB.child(key).child("w_status").child("status").setValue("Complete");
                        }else {
                            mTaskDB.child(key).child("w_status").child("status").setValue("Incomplete");
                        }
                    }
                });
                checkBox_harvest.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked){
                            mTaskDB.child(key).child("h_status").child("status").setValue("Complete");
                        }else {
                            mTaskDB.child(key).child("h_status").child("status").setValue("Incomplete");
                        }
                    }
                });

                // ---------- LAND PREP ----------
                if (lp_status != null) {
                    if (lp_status.equals("Complete")) {
                        checkBox_land_prep.setChecked(true);
                        Common.checkedBox(mtxtLPS_time, mtxtLPE_time, mtxtLP_desc);
                        //checkBox_seeds.setEnabled(true);
                    } else{
                        checkBox_land_prep.setChecked(false);
                        Common.uncheckedBox(mtxtLPS_time, mtxtLPE_time, mtxtLP_desc);
                        //checkBox_seeds.setEnabled(false);
                    }
                }
                // ---------- SEED SELECTION ----------
                if (s_status != null) {
                    if (s_status.equals("Complete")) {
                        checkBox_seeds.setChecked(true);
                        Common.checkedBox(mtxtSS_time_start, mtxtSS_time_end, mtxtSS_desc);
                    } else{
                        checkBox_seeds.setChecked(false);
                        Common.uncheckedBox(mtxtSS_time_start, mtxtSS_time_end, mtxtSS_desc);
                    }
                }
                // ---------- PLANT ----------
                if (p_status != null) {
                    if (p_status.equals("Complete")) {
                        checkBox_plant.setChecked(true);
                        Common.checkedBox(mtxtP_time_start, mtxtP_time_end, mtxtP_desc);
                    } else{
                        checkBox_plant.setChecked(false);
                        Common.uncheckedBox(mtxtP_time_start, mtxtP_time_end, mtxtP_desc);
                    }
                }
                // ---------- FERTILIZER ----------
                if (f_status != null) {
                    if (f_status.equals("Complete")) {
                        checkBox_fertilizer.setChecked(true);
                        Common.checkedBox(mtxtF_time_start, mtxtF_time_end, mtxtF_desc);
                    } else{
                        checkBox_fertilizer.setChecked(false);
                        Common.uncheckedBox(mtxtF_time_start, mtxtF_time_end, mtxtF_desc);
                    }
                }
                // ---------- WEED ----------
                if (w_status != null) {
                    if (w_status.equals("Complete")) {
                        checkBox_weed.setChecked(true);
                        Common.checkedBox(mtxtW_time_start, mtxtW_time_end, mtxtW_desc);
                    } else{
                        checkBox_weed.setChecked(false);
                        Common.uncheckedBox(mtxtW_time_start, mtxtW_time_end, mtxtW_desc);
                    }
                }
                // ---------- HARVEST ----------
                if (h_status != null) {
                    if (h_status.equals("Complete")) {
                        checkBox_harvest.setChecked(true);
                        Common.checkedBox(mtxtH_time_start, mtxtH_time_end, mtxtH_desc);
                    } else{
                        checkBox_harvest.setChecked(false);
                        Common.uncheckedBox(mtxtH_time_start, mtxtH_time_end, mtxtH_desc);
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTxtTitle.setText("Planning");
        //imgBtnMap.setVisibility(View.VISIBLE);
        mImgProf.setVisibility(View.VISIBLE);
        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnBack.setVisibility(View.GONE);
    }
}
