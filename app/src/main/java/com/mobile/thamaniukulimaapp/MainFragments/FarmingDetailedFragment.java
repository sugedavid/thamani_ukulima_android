package com.mobile.thamaniukulimaapp.MainFragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.thamaniukulimaapp.Adapter.WeatherForecastAdapter;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.Model.WeatherForecastResult;
import com.mobile.thamaniukulimaapp.R;
import com.mobile.thamaniukulimaapp.Retrofit.IOpenWeatherMap;
import com.mobile.thamaniukulimaapp.Retrofit.RetrofitClient;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class FarmingDetailedFragment extends Fragment {

    private View itemView;
    private TextView mTxtTitle;
    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;
    private String duration, condition1, condition2, depth, spacing ;
    private CompositeDisposable compositeDisposable;
    private IOpenWeatherMap mService;
    private static FarmingDetailedFragment instance;

    private RecyclerView recycler_forecast;
    private String userID, farmID, input, crop, activity;
    private double dbLat, dbLng;
    private FirebaseAuth mAuth;
    private DatabaseReference mLandPrepDB;


    public static FarmingDetailedFragment getInstance(){
        if (instance == null)
            instance = new FarmingDetailedFragment();
        return instance;
    }


    public FarmingDetailedFragment() {
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getInstance();
        mService = retrofit.create(IOpenWeatherMap.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        itemView =  inflater.inflate(R.layout.fragment_farming_detailed, container, false);

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnMap  = getActivity().findViewById(R.id.imgBtnMap);
        mImgProf = getActivity().findViewById(R.id.imgProf);
        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);
        imgBtnBack.setVisibility(View.VISIBLE);
        imgBtnMap.setVisibility(View.GONE);
        mImgProf.setVisibility(View.GONE);
        imgBtnSort.setVisibility(View.GONE);

        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();

        if (getArguments() != null) {
            dbLat = getArguments().getDouble("lat");
            dbLng = getArguments().getDouble("lng");
            farmID = getArguments().getString("farmID");
            crop = getArguments().getString("crop");
            activity = getArguments().getString("activity");
            mTxtTitle.setText(activity);
        }
        mLandPrepDB = FirebaseDatabase.getInstance().getReference()
                .child("FarmingActivity").child(crop).child(activity);
        // ------------------ GET LAND PREP DETAILS ----------------------
        mLandPrepDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

//                duration = (String) dataSnapshot.child("Duration").getValue();
                condition1 = (String) dataSnapshot.child("Condition").child("1").getValue();
                condition2 = (String) dataSnapshot.child("Condition").child("2").getValue();
//                depth = (String) dataSnapshot.child("Specification").child("Depth").getValue();
//                spacing = (String) dataSnapshot.child("Specification").child("Spacing").getValue();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        recycler_forecast = itemView.findViewById(R.id.recycler_forecast_l);
        recycler_forecast.setHasFixedSize(true);
        recycler_forecast.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL,false));

        getForecastWeatherInformation();


        return itemView;
    }

    private void getForecastWeatherInformation() {

            compositeDisposable.add(mService.getForecastWeatherByLatLng(
                    String.valueOf(dbLat),
                    String.valueOf(dbLng),
                    Common.API_KEY,
                    "metric")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<WeatherForecastResult>() {
                        @Override
                        public void accept(WeatherForecastResult weatherForecastResult) throws Exception {
                            displayForecastWeather(weatherForecastResult, farmID, condition1, condition2, crop);

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Snackbar.make(getActivity().findViewById(android.R.id.content), throwable.getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    })
            );
    }

    private void displayForecastWeather(WeatherForecastResult weatherForecastResult, String fID, String weatherdesc, String weatherdesc2,
                                        String crop) {
        WeatherForecastAdapter adapter = new WeatherForecastAdapter(getContext(),weatherForecastResult, fID, weatherdesc, weatherdesc2, crop);
        recycler_forecast.setAdapter(adapter);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mTxtTitle.setText("Home");
        imgBtnMap.setVisibility(View.VISIBLE);
        mImgProf.setVisibility(View.VISIBLE);
        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnBack.setVisibility(View.GONE);
    }



}
