package com.mobile.thamaniukulimaapp.MainFragments;

public class Model {

    private String desc_m;
    private String time_m;

    public Model(){

    }

    public Model(String desc, String time){

        this.desc_m = desc;
        this.time_m = time;
    }

    public String getPaymentDesc(){
        return desc_m;
    }
    public void setPaymentDesc(String desc){
        this.desc_m = desc;
    }
    public String getTime(){
        return time_m;
    }
    public void setTime(String time){
        this.time_m = time;
    }

}
