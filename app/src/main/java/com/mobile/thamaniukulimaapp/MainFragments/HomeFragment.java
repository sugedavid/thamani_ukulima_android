package com.mobile.thamaniukulimaapp.MainFragments;


import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobile.thamaniukulimaapp.AddFarmActivity;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.MainActivity;
import com.mobile.thamaniukulimaapp.Model.WeatherResult;
import com.mobile.thamaniukulimaapp.R;
import com.mobile.thamaniukulimaapp.Retrofit.IOpenWeatherMap;
import com.mobile.thamaniukulimaapp.Retrofit.RetrofitClient;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private TextView mTxtTitle, mTxtUsername, mTxtFarmLoc, mTxtWeatherDesc, mTxtTemp, mTxtCrop;
    private ImageView imgWeatherIcon;
    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;
    private CardView cdvFav, cdvLand, cdvSeeds, cdvPlant, cdvFertilizer, cdvWeed, cdvHarvest;
    private DatabaseReference mCustomerDatabase;
    private FirebaseAuth mAuth;
    private String userID;

    private CompositeDisposable compositeDisposable;
    private IOpenWeatherMap mService;
    private static HomeFragment instance;
    private double dbLat, dbLng;
    private ProgressBar loading;


    public static HomeFragment getInstance(){
        if (instance == null)
            instance = new HomeFragment();
        return instance;
    }

    public HomeFragment() {
        // Required empty public constructor
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getInstance();
        mService = retrofit.create(IOpenWeatherMap.class);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View itemView = inflater.inflate(R.layout.fragment_home, container, false);

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnMap  = getActivity().findViewById(R.id.imgBtnMap);
        mImgProf = getActivity().findViewById(R.id.imgProf);
        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);
//        mTxtTitle.setText("Select Activity");
//        imgBtnBack.setVisibility(View.VISIBLE);
//        imgBtnMap.setVisibility(View.GONE);
//        imgBtnSort.setVisibility(View.GONE);

        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getContext(), AddFarmActivity.class));
            }
        });

        cdvLand  = itemView.findViewById(R.id.cdvLand);
        cdvSeeds  = itemView.findViewById(R.id.cdvSeeds);
        cdvPlant  = itemView.findViewById(R.id.cdvPlant);
        cdvFertilizer  = itemView.findViewById(R.id.cdvFertilizer);
        cdvWeed  = itemView.findViewById(R.id.cdvWeed);
        cdvHarvest  = itemView.findViewById(R.id.cdvHarvest);
        cdvFav  = itemView.findViewById(R.id.cdvFav);
        mTxtUsername = itemView.findViewById(R.id.txtUsername);
        mTxtFarmLoc = itemView.findViewById(R.id.txtFarmLoc);
        mTxtWeatherDesc = itemView.findViewById(R.id.txtWeatherDesc);
        mTxtTemp = itemView.findViewById(R.id.txtTemp);
        mTxtCrop = itemView.findViewById(R.id.txtCrop);
        imgWeatherIcon = itemView.findViewById(R.id.imgWeatherIcon);
        loading = itemView.findViewById(R.id.loadingF);

        mTxtFarmLoc.setPaintFlags(mTxtFarmLoc.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTxtCrop.setPaintFlags(mTxtCrop.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        mTxtFarmLoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction;
                transaction = getParentFragmentManager().beginTransaction();
                transaction.replace(R.id.frameLayout, new MyFarmsFragment());
                transaction.commit();
                transaction.addToBackStack(null);
                imgBtnMap.setVisibility(View.GONE);
                mImgProf.setVisibility(View.GONE);
                mTxtTitle.setText("My Farms");
                imgBtnBack.setVisibility(View.VISIBLE);
            }
        });


        mAuth = FirebaseAuth.getInstance();
        userID = mAuth.getCurrentUser().getUid();
        mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);

        mCustomerDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if(dataSnapshot.child("username").exists()){
                    String mUsername = dataSnapshot.child("username").getValue().toString();
                    mTxtUsername.setText("Welcome " + mUsername + ",");
                }else {
                    mTxtUsername.setText("Welcome,");
                }

                if (dataSnapshot.child("Favorite").child("Latitude").exists()
                        && dataSnapshot.child("Favorite").child("Longitude").exists()
                        && dataSnapshot.child("Favorite").child("FarmID").exists()
                        && dataSnapshot.child("Favorite").child("Crop").exists()){

                    dbLat = (double) dataSnapshot.child("Favorite").child("Latitude").getValue();
                    dbLng = (double) dataSnapshot.child("Favorite").child("Longitude").getValue();
                    String farmID =  dataSnapshot.child("Favorite").child("FarmID").getValue().toString();
                    String crop =  dataSnapshot.child("Favorite").child("Crop").getValue().toString();


                    getWeatherInformation();

                    // ----- FARMING DETAILED -----------
                    cdvLand.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openFarmingDetailed(v, dbLat, dbLng, farmID, crop, "LandPrep");
                        }
                    });
                    cdvSeeds.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openFarmingDetailed(v, dbLat, dbLng, farmID, crop, "Seeds");
                        }
                    });
                    cdvPlant.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openFarmingDetailed(v, dbLat, dbLng, farmID, crop, "Plant");
                        }
                    });
                    cdvFertilizer.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openFarmingDetailed(v, dbLat, dbLng, farmID, crop, "Fertilizer");
                        }
                    });
                    cdvWeed.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openFarmingDetailed(v, dbLat, dbLng, farmID, crop, "Weed");
                        }
                    });
                    cdvHarvest.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openFarmingDetailed(v, dbLat, dbLng, farmID, crop, "Harvest");
                        }
                    });

                    cdvFav.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            imgBtnBack.setVisibility(View.VISIBLE);
                            //mTxtTitle.setText(mTxtFarmLoc.getText());
                            Bundle bundle = new Bundle();
                            bundle.putDouble("lat", dbLat);
                            bundle.putDouble("lng", dbLng);
                            bundle.putString("farmID", farmID);
                            bundle.putString("crop", crop);
                            WeatherInfoFragment weatherInfoFragment = new WeatherInfoFragment();
                            weatherInfoFragment.setArguments(bundle);

                            FragmentTransaction transaction;
                            transaction = getParentFragmentManager().beginTransaction();
                            transaction.addToBackStack(null);
                            transaction.replace(R.id.frameLayout, weatherInfoFragment);
                            transaction.commit();


                        }
                    });

                }else {
                    loading.setVisibility(View.GONE);
                    Snackbar.make(getActivity().findViewById(android.R.id.content), "No favorite farm", Snackbar.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return itemView;


    }

    private void getWeatherInformation() {

            compositeDisposable.add(mService.getWeatherByLatLng(
                    String.valueOf(dbLat),
                    String.valueOf(dbLng),
                    Common.API_KEY,
                    "metric")
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<WeatherResult>() {
                        @Override
                        public void accept(WeatherResult weatherResult) throws Exception {
                            //Load Image
                            Picasso.get().load(new StringBuilder("http://openweathermap.org/img/w/")
                                    .append(weatherResult.getWeather().get(0).getIcon())
                                    .append(".png").toString()).into(imgWeatherIcon);

                            mTxtFarmLoc.setText(weatherResult.getName());
                            mTxtWeatherDesc.setText(weatherResult.getWeather().get(0).getDescription());
                            /*txt_description.setText(new StringBuilder("Weather in ")
                                    .append(weatherResult.getName()).toString());*/
                            mTxtTemp.setText(new StringBuilder(String.valueOf(weatherResult.getMain().getTemp()))
                                    .append("°C").toString());
                            //txt_date_time.setText("Updated "+Common.convertUnixToDate(weatherResult.getDt()));

                            loading.setVisibility(View.GONE);

                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            loading.setVisibility(View.GONE);
                            Snackbar.make(getActivity().findViewById(android.R.id.content), throwable.getMessage(), Snackbar.LENGTH_LONG).show();
                        }
                    }));
        }


    @Override
    public void onDestroy() {
        super.onDestroy();
//        mTxtTitle.setText("Home");
//        imgBtnMap.setVisibility(View.VISIBLE);
//        imgBtnSort.setVisibility(View.VISIBLE);
//        imgBtnBack.setVisibility(View.GONE);
    }

}
