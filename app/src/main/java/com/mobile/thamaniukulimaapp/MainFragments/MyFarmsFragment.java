package com.mobile.thamaniukulimaapp.MainFragments;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mobile.thamaniukulimaapp.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyFarmsFragment extends Fragment {

    private View mview;
    private TextView mTxtAddFarm, mTxtFarmAct;
    private DatabaseReference mTaskDB, mCustomerDatabase;
    private FirebaseAuth mAuth;
    private RecyclerView mRvTaskHist;
    private Query mQueryTaskHist;

    private TextView mTxtTitle, mTxtFarmCrop;
    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;



    public MyFarmsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_my_farms, container, false);

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnMap  = getActivity().findViewById(R.id.imgBtnMap);
        mImgProf = getActivity().findViewById(R.id.imgProf);
        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);
        mTxtTitle.setText("My Farms");
        imgBtnBack.setVisibility(View.VISIBLE);
        imgBtnMap.setVisibility(View.GONE);
        mImgProf.setVisibility(View.GONE);
        imgBtnSort.setVisibility(View.GONE);

        mAuth = FirebaseAuth.getInstance();
        String userID = mAuth.getCurrentUser().getUid();
        mTaskDB = FirebaseDatabase.getInstance().getReference().child("Users").child(userID)
                .child("MyFarms");
        mTaskDB.keepSynced(true);
        mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);
        mCustomerDatabase.keepSynced(true);

        mTxtAddFarm = mview.findViewById(R.id.txtAddFarm);
        mTxtFarmAct = mview.findViewById(R.id.txt_farm_act_h);


        mTaskDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    mTxtAddFarm.setVisibility(View.GONE);
                }else {
                    mTxtAddFarm.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mQueryTaskHist = mTaskDB.orderByChild("PlaceName");

        mRvTaskHist = mview.findViewById(R.id.rv_farm);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        mRvTaskHist.setHasFixedSize(true);
        mRvTaskHist.setLayoutManager(layoutManager);



        return  mview;
    }

    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerAdapter<Model, ModelViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Model, ModelViewHolder>(

                Model.class,
                R.layout.item_farms,
                ModelViewHolder.class,
                mTaskDB
        )
        {
            @Override
            protected void populateViewHolder(final ModelViewHolder viewHolder, Model model, int position) {

                final String post_key = getRef(position).getKey();

                if (post_key!=null){


                    mTaskDB.child(post_key).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            final String desc = (String) dataSnapshot.child("Description").getValue();
                            final String name = (String) dataSnapshot.child("PlaceName").getValue();
                            final double lat = (double) dataSnapshot.child("Latitude").getValue();
                            final double lng = (double) dataSnapshot.child("Longitude").getValue();

                            viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {

                                    CharSequence options[] = new CharSequence[]{"Delete Farm"};

                                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                                    builder.setItems(options, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {

                                            if (i == 0) {

                                                new AlertDialog.Builder(getContext()).setIcon(R.drawable.ic_delete_black)
                                                        .setTitle("Delete Farm").setMessage("Are you sure you want to Delete this Farm?")
                                                        .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                                                            @Override
                                                            public void onClick(DialogInterface dialog, int which) {

                                                                mTaskDB.child(post_key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        if (task.isSuccessful()){

                                                                            Toast.makeText(getContext(), "Farm Deleted", Toast.LENGTH_SHORT).show();

                                                                        }
                                                                    }
                                                                });

                                                            }
                                                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        dialog.dismiss();
                                                    }
                                                }).show();


                                            }


                                        }
                                    });

                                    builder.show();

                                    return true;

                                }
                            });

                            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {


//                                    imgBtnMap.setVisibility(View.GONE);
//                                    mTxtTitle.setText("Map");
//                                    imgBtnBack.setVisibility(View.VISIBLE);

                                    mCustomerDatabase.child("Favorite").child("Latitude").setValue(lat);
                                    mCustomerDatabase.child("Favorite").child("Longitude").setValue(lng);
                                    mCustomerDatabase.child("Favorite").child("FarmID").setValue(post_key).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {

                                            if (task.isSuccessful()){
                                                FragmentManager fm = getParentFragmentManager();
                                                FragmentTransaction ft=fm.beginTransaction();
                                                if(fm.getBackStackEntryCount()>0) {
                                                    fm.popBackStack();
                                                }

                                                ft.commit();

                                                mTxtTitle.setText("Home");
                                                //imgBtnMap.setVisibility(View.VISIBLE);
                                                mImgProf.setVisibility(View.VISIBLE);
                                                imgBtnSort.setVisibility(View.VISIBLE);
                                                imgBtnBack.setVisibility(View.GONE);

                                                Toast.makeText(getContext(), "Default Farm changed to " + name, Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });



//                                    Bundle bundle = new Bundle();
//                                    bundle.putDouble("lat", lat);
//                                    bundle.putDouble("lng", lng);
//                                    bundle.putString("farmID", post_key);
//                                    WeatherInfoFragment weatherInfoFragment = new WeatherInfoFragment();
//                                    weatherInfoFragment.setArguments(bundle);
//
//                                    FragmentTransaction transaction;
//                                    transaction = getParentFragmentManager().beginTransaction();
//                                    transaction.addToBackStack(null);
//                                    transaction.replace(R.id.frameLayout, weatherInfoFragment);
//                                    transaction.commit();

//                                    getParentFragmentManager().beginTransaction().add(R.id.frameLayout, weatherInfoFragment).commit();

                                }
                            });

                            if (desc!=null){
                                viewHolder.mTxtDesc.setText(desc);
                            }

                            if (name!=null){
                                viewHolder.mTxtName.setText(name);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }


            }
        };

        mRvTaskHist.setAdapter(firebaseRecyclerAdapter);

    }



    public static class ModelViewHolder extends RecyclerView.ViewHolder{

        TextView mTxtDesc, mTxtName;
        View mView;


        public ModelViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mTxtDesc = mView.findViewById(R.id.txtFarmdesc);
            mTxtName = mView.findViewById(R.id.txtFarmName);


        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTxtTitle.setText("Home");
        //imgBtnMap.setVisibility(View.VISIBLE);
        mImgProf.setVisibility(View.VISIBLE);
        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnBack.setVisibility(View.GONE);
    }

}
