package com.mobile.thamaniukulimaapp.MainFragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mobile.thamaniukulimaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationFragment extends Fragment {

    private View mview;
    private TextView mTxtAddNot;
    private DatabaseReference mTaskDB;
    private FirebaseAuth mAuth;
    private RecyclerView mRvTaskHist;
    private Query mQueryTaskHist;

    private ImageButton imgBtnSort;


    public NotificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_notification, container, false);

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnSort.setVisibility(View.GONE);

        mAuth = FirebaseAuth.getInstance();
        String userID = mAuth.getCurrentUser().getUid();
        mTaskDB = FirebaseDatabase.getInstance().getReference().child("items");
        mTaskDB.keepSynced(true);

        mTxtAddNot = mview.findViewById(R.id.txtAddNot);

        mTaskDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    mTxtAddNot.setVisibility(View.GONE);
                }else {
                    mTxtAddNot.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

                mQueryTaskHist = mTaskDB.orderByChild("Date");

        mRvTaskHist = mview.findViewById(R.id.rv_not);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        mRvTaskHist.setHasFixedSize(true);
        mRvTaskHist.setLayoutManager(layoutManager);

        return mview;
    }

    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerAdapter<Model, ModelViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Model, ModelViewHolder>(

                Model.class,
                R.layout.item_notifications,
                ModelViewHolder.class,
                mTaskDB
        )
        {
            @Override
            protected void populateViewHolder(final ModelViewHolder viewHolder, Model model, int position) {

                final String post_key = getRef(position).getKey();

                if (post_key!=null){


                    mTaskDB.child(post_key).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            final String title = (String) dataSnapshot.child("title").getValue();
                            final String body = (String) dataSnapshot.child("body").getValue();

                            if (title!=null){
                                viewHolder.mTxtDesc.setText(title);
                            }

                            if (body!=null){
                                viewHolder.mTxtTime.setText(body);
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });


                }


            }
        };

        mRvTaskHist.setAdapter(firebaseRecyclerAdapter);

    }



    public static class ModelViewHolder extends RecyclerView.ViewHolder{

        TextView mTxtDesc, mTxtTime;
        View mView;


        public ModelViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mTxtDesc = mView.findViewById(R.id.txtNotdesc);
            mTxtTime =mView.findViewById(R.id.txtNottime);


        }

    }

}
