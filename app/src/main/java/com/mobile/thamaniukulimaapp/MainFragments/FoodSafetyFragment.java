package com.mobile.thamaniukulimaapp.MainFragments;


import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobile.thamaniukulimaapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodSafetyFragment extends Fragment {

    private View mView;
    private CardView mCdPlanting, mCdSafety;


    public FoodSafetyFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView =  inflater.inflate(R.layout.fragment_food_safety, container, false);

        mCdPlanting = mView.findViewById(R.id.cdPlanting);
        mCdSafety = mView.findViewById(R.id.cdSafety);

        mCdPlanting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Info on Planting Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });

        mCdSafety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "Info on Food Safety Coming Soon", Toast.LENGTH_SHORT).show();
            }
        });

        return mView;
    }

}
