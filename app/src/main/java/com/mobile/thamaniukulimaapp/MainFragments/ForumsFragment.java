package com.mobile.thamaniukulimaapp.MainFragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.FirebaseModel.ForumModel;
import com.mobile.thamaniukulimaapp.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForumsFragment extends Fragment {

    private View mView;
    private FloatingActionButton mFbCreatePost;

    private TextView mTxtTitle;
    private ImageButton imgBtnBack, mImgPost, imgBtnSort;

    private DatabaseReference mDtbPostForum, mDtbUser;
    private Query mQueryForum;
    private String userID;
    private RecyclerView mRvForum;

    public ForumsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_forums, container, false);

        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);

        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction;
                transaction = getParentFragmentManager().beginTransaction();
                transaction.replace(R.id.frameLayout, new CreatePostFragment());
                transaction.commit();
                transaction.addToBackStack(null);

                mTxtTitle.setText("Create a Post");
                imgBtnBack.setVisibility(View.VISIBLE);
            }
        });

        mFbCreatePost = mView.findViewById(R.id.fbCreatePost);
        mRvForum = mView.findViewById(R.id.rvForums);
        mFbCreatePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction;
                transaction = getParentFragmentManager().beginTransaction();
                transaction.replace(R.id.frameLayout, new CreatePostFragment());
                transaction.commit();
                transaction.addToBackStack(null);

                mTxtTitle.setText("Create a Post");
                imgBtnBack.setVisibility(View.VISIBLE);
            }
        });

        userID = Common.getUserID();
        mDtbPostForum = FirebaseDatabase.getInstance().getReference().child("Forums");
        mDtbPostForum.keepSynced(true);
        mQueryForum = mDtbPostForum.orderByChild("time");

        mDtbUser = FirebaseDatabase.getInstance().getReference().child("Users");
        mDtbUser.keepSynced(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        mRvForum.setHasFixedSize(true);
        mRvForum.setLayoutManager(layoutManager);

        return mView;
    }

    @Override
    public void onStart() {
        super.onStart();
            FirebaseRecyclerAdapter<ForumModel, ModelViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<ForumModel, ModelViewHolder>(

                ForumModel.class,
                R.layout.item_forums,
                ModelViewHolder.class,
                mQueryForum
        )
        {
            @Override
            protected void populateViewHolder(final ModelViewHolder viewHolder, ForumModel model, int position) {

                final String post_key = getRef(position).getKey();

                if (post_key!=null){

                    // FORUM INFO
                    mDtbPostForum.child(post_key).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            if (dataSnapshot.child("forumText").exists()) {
                                viewHolder.mTxtForumDesc.setVisibility(View.VISIBLE);
                                viewHolder.mTxtForumDesc.setText((String) dataSnapshot.child("forumText").getValue());

                            }else {
                                viewHolder.mTxtForumDesc.setVisibility(View.GONE);
                            }
                            if (dataSnapshot.child("time").exists()) {
                                viewHolder.mTxtDate.setText(Common.getTime((long) dataSnapshot.child("time").getValue()));
                            }
                            if (dataSnapshot.child("forumImageUrl").exists()){
                                viewHolder.imgForum.setVisibility(View.VISIBLE);
                                String mImageForumUrl = (String) dataSnapshot.child("forumImageUrl").getValue();
                                Picasso.get().load(mImageForumUrl).networkPolicy(NetworkPolicy.OFFLINE)
                                        .placeholder(R.drawable.input_outline_white).into(viewHolder.imgForum, new Callback() {
                                    @Override
                                    public void onSuccess() {
                                    }

                                    @Override
                                    public void onError(Exception e) {
                                        Picasso.get().load(mImageForumUrl).placeholder(R.drawable.input_outline_white).into(viewHolder.imgForum);
                                    }

                                });
                            }else {
                                viewHolder.imgForum.setVisibility(View.GONE);
                            }
                            if (dataSnapshot.child("userID").exists()) {
                                String id = (String) dataSnapshot.child("userID").getValue();

                                // USER INFO
                                mDtbUser.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                        if (dataSnapshot.child("username").exists()){
                                            viewHolder.mTxtName.setText( (String) dataSnapshot.child("username").getValue());
                                        }
                                        if (dataSnapshot.child("profileImageUrl").exists()){
                                            String mProfileImageUrl = (String) dataSnapshot.child("profileImageUrl").getValue();
                                            Picasso.get().load(mProfileImageUrl).networkPolicy(NetworkPolicy.OFFLINE)
                                                    .placeholder(R.drawable.userprof).into(viewHolder.imgProf, new Callback() {
                                                @Override
                                                public void onSuccess() {
                                                }

                                                @Override
                                                public void onError(Exception e) {
                                                    Picasso.get().load(mProfileImageUrl).placeholder(R.drawable.userprof).into(viewHolder.imgProf);
                                                }

                                            });
                                        }

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                }

            }
        };

        mRvForum.setAdapter(firebaseRecyclerAdapter);

    }

    public static class ModelViewHolder extends RecyclerView.ViewHolder{

        TextView mTxtForumDesc, mTxtDate, mTxtName;
        View mView;
        CircleImageView imgProf;
        ImageView imgForum;
        ImageButton likeButton;


        public ModelViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mTxtForumDesc = mView.findViewById(R.id.txtDescForum);
            mTxtDate =mView.findViewById(R.id.txtDateForum);
            mTxtName =mView.findViewById(R.id.txtNameForum);
            imgProf = mView.findViewById(R.id.imgProfForum);
            imgForum = mView.findViewById(R.id.imgForum);
            likeButton = mView.findViewById(R.id.imgLike);

        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        imgBtnSort.setVisibility(View.VISIBLE);
    }
}
