package com.mobile.thamaniukulimaapp.MainFragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mikhaellopez.circularprogressbar.CircularProgressBar;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.MainActivity;
import com.mobile.thamaniukulimaapp.R;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalenderFragment extends Fragment {

    private View mview;
    private TextView mTxtAddTask;
    private Button mBtnSaveEvent;
    private DatabaseReference mTaskDB;
    private FirebaseAuth mAuth;
    private RecyclerView mRvTaskHist;
    private Query mQueryTaskHist;

    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;
    private TextView mTxtTitle;
    private Bundle bundle = new Bundle();
    private String lp_desc;
    private Long lps_time, lpe_time;
    private long ls, le, se, pe, fe, we, he;



    public CalenderFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview = inflater.inflate(R.layout.fragment_calender, container, false);

        imgBtnSort  = getActivity().findViewById(R.id.imgBtnSort);
        imgBtnMap  = getActivity().findViewById(R.id.imgBtnMap);
        mImgProf = getActivity().findViewById(R.id.imgProf);
        imgBtnBack  = getActivity().findViewById(R.id.imgBtnBack);
        mTxtTitle  = getActivity().findViewById(R.id.txtTitle);

        imgBtnSort.setVisibility(View.VISIBLE);
        imgBtnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialogButtonClicked();
            }
        });

        mAuth = FirebaseAuth.getInstance();
        String userID = mAuth.getCurrentUser().getUid();
        mTaskDB = FirebaseDatabase.getInstance().getReference().child("Tasks").child(userID);
        mTaskDB.keepSynced(true);

        mTxtAddTask = mview.findViewById(R.id.txtAddTask);

        mTaskDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    mTxtAddTask.setVisibility(View.GONE);
                }else {
                    mTxtAddTask.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mQueryTaskHist = mTaskDB.orderByChild("ls");
        mRvTaskHist = mview.findViewById(R.id.rv_task_history);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        layoutManager.setStackFromEnd(true);
        mRvTaskHist.setHasFixedSize(true);
        mRvTaskHist.setLayoutManager(layoutManager);

        return mview;
    }

    public void showAlertDialogButtonClicked() {

        //String randID = UUID.randomUUID().toString();
        String randID = mTaskDB.push().getKey();
        Date date = new Date();
        ls = date.getTime();

        // custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.task_main_layout, null);
        CalendarView calendar = customLayout.findViewById(R.id.calendarView);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            //show the selected date as a toast
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
                Toast.makeText(getActivity(), day + "/" + month + "/" + year, Toast.LENGTH_SHORT).show();
                Calendar c = Calendar.getInstance();
                c.set(year, month, day);
                ls = c.getTimeInMillis(); //this is what you want to use later
                //ls = calendar.getDate();
                le = ls + 864000000;
                se = le + 86400000;
                pe = se + 604800000;
                fe = pe + 1814400000;
                we = fe + 2419200000L;
                he = we + 86400000;
            }
        });
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getContext());
        builder.setTitle("Farm Planner");
        builder.setView(customLayout);

        // add a button
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // ----------------- POST TASK TO DB -----------------------
                EditText mEdtTask = customLayout.findViewById(R.id.edtTaskMain);
                String task = mEdtTask.getText().toString().trim();

                mTaskDB.child(randID).child("Crop").setValue("Maize");
                mTaskDB.child(randID).child("ls").setValue(ls);
                mTaskDB.child(randID).child("le").setValue(le);
                mTaskDB.child(randID).child("se").setValue(se);
                mTaskDB.child(randID).child("pe").setValue(pe);
                mTaskDB.child(randID).child("fe").setValue(fe);
                mTaskDB.child(randID).child("we").setValue(we);
                mTaskDB.child(randID).child("he").setValue(he);
                mTaskDB.child(randID).child("lp_status").child("status").setValue("Incomplete");
                mTaskDB.child(randID).child("s_status").child("status").setValue("Incomplete");
                mTaskDB.child(randID).child("p_status").child("status").setValue("Incomplete");
                mTaskDB.child(randID).child("f_status").child("status").setValue("Incomplete");
                mTaskDB.child(randID).child("w_status").child("status").setValue("Incomplete");
                mTaskDB.child(randID).child("h_status").child("status").setValue("Incomplete").addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(getContext(), "Farming Planned", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        // create and show the alert dialog
        androidx.appcompat.app.AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onStart() {
        super.onStart();


        FirebaseRecyclerAdapter<Model, ModelViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Model, ModelViewHolder>(

                Model.class,
                R.layout.item_task_history,
                ModelViewHolder.class,
                mQueryTaskHist
        )
        {
            @Override
            protected void populateViewHolder(final ModelViewHolder viewHolder, Model model, int position) {

                final String post_key = getRef(position).getKey();

                if (post_key!=null){

                    viewHolder.circularProgressBar.setColor(ContextCompat.getColor(getActivity(), R.color.progressBarColor));
                    viewHolder.circularProgressBar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.backgroundProgressBarColor));
                    viewHolder.circularProgressBar.setProgressBarWidth(getResources().getDimension(R.dimen.progressBarWidth));
                    viewHolder.circularProgressBar.setBackgroundProgressBarWidth(getResources().getDimension(R.dimen.backgroundProgressBarWidth));
                    int animationDuration = 1500; // 2500ms = 2,5s
                    viewHolder. circularProgressBar.setProgressWithAnimation(65, animationDuration); // Default duration = 1500ms


                    viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {

                            CharSequence options[] = new CharSequence[]{"Delete Farm Plan"};

                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setItems(options, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    if (i == 0) {

                                        new AlertDialog.Builder(getContext()).setIcon(R.drawable.ic_delete_black)
                                                .setTitle("Delete Farm Plan").setMessage("Are you sure you want to Delete this Plan?")
                                                .setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {

                                                        mTaskDB.child(post_key).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                if (task.isSuccessful()){
                                                                    Toast.makeText(getContext(), "Farm Plan Deleted", Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        });
                                                    }
                                                }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                                dialog.dismiss();
                                            }
                                        }).show();
                                    }
                                }
                            });

                            builder.show();

                            return true;

                        }
                    });

                    Query taskQuery = mTaskDB.child(post_key).orderByChild("status").equalTo("Complete");
                    taskQuery.keepSynced(true);
                    taskQuery.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            float progress;
                            float status_count = dataSnapshot.getChildrenCount();
                            int animationDuration = 2500;
                            progress = status_count/6 * 100;
                            int final_progress = (int) progress;
                            viewHolder.circularProgressBar.setProgressWithAnimation(progress, animationDuration);
                            viewHolder.mTxtProgressPercentage.setText(String.valueOf(new DecimalFormat("#").format(progress)));
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    mTaskDB.child(post_key).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            String ls, le, se, pe, fe, we;
                            String lp_status = (String) dataSnapshot.child("lp_status").child("status").getValue();
                            String s_status = (String) dataSnapshot.child("s_status").child("status").getValue();
                            String p_status = (String) dataSnapshot.child("p_status").child("status").getValue();
                            String f_status = (String) dataSnapshot.child("f_status").child("status").getValue();
                            String w_status = (String) dataSnapshot.child("w_status").child("status").getValue();
                            String h_status = (String) dataSnapshot.child("h_status").child("status").getValue();
                            //viewHolder.mTxtDesc.setText("Land Preparation");

                            if (dataSnapshot.child("ls").exists()) {
                                viewHolder.mTxtTime.setText(Common.getTime((long) dataSnapshot.child("ls").getValue()));

                            }
                            if (dataSnapshot.child("he").exists()) {
                                viewHolder.mTxtEndDate.setText(Common.getTime((long) dataSnapshot.child("he").getValue()));
                            }

                            if (dataSnapshot.child("Crop").exists()){
                                viewHolder.mTxtCrop.setText(dataSnapshot.child("Crop").getValue().toString());
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            imgBtnBack.setVisibility(View.VISIBLE);
                            mTxtTitle.setText("Farm Timeline");

                            FarmTimelineFragment farmTimelineFragment = new FarmTimelineFragment();
                            bundle.putString("key", post_key);
                            farmTimelineFragment.setArguments(bundle);

                            FragmentTransaction transaction;
                            transaction = getParentFragmentManager().beginTransaction();
                            transaction.addToBackStack(null);
                            transaction.replace(R.id.frameLayout, farmTimelineFragment);
                            transaction.commit();
                        }
                    });


                }


            }
        };

        mRvTaskHist.setAdapter(firebaseRecyclerAdapter);

    }

    public static class ModelViewHolder extends RecyclerView.ViewHolder{

        TextView mTxtDesc, mTxtTime, mTxtCrop, mTxtEndDate, mTxtProgressPercentage;
        View mView;
        CircularProgressBar circularProgressBar;


        public ModelViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

            mTxtDesc = mView.findViewById(R.id.txtTaskdesc);
            mTxtTime =mView.findViewById(R.id.txtTasktime);
            mTxtCrop =mView.findViewById(R.id.txtCropTask);
            mTxtEndDate =mView.findViewById(R.id.txtEndDate);
            mTxtProgressPercentage =mView.findViewById(R.id.txtProgressPercentage);
            circularProgressBar = mView.findViewById(R.id.yourCircularProgressbar);

        }

    }

}
