package com.mobile.thamaniukulimaapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {

    private DatabaseReference mRootRef,mUserRef, mDtbUnread, mMinusRead;
    private Query mQuery;
    private ImageView mInfo;

    private FirebaseAuth mAuth;
    private String mCurrentUserId;

    private ImageButton mChatSendBtn,mChatAttachBtn;
    private EditText mChatMessageView, emojiconEditText;
    View rootView;

    private RecyclerView mMessagesList;
    private final List<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager mLinearLayout;
    private MessageAdapter mAdapter;
    private StorageReference mImageStorage;

    private static final int GALLERY_REQUEST = 2;
    private static final int REQUEST_TAKE_PHOTO = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {

            mUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());
        }

        mCurrentUserId = mAuth.getCurrentUser().getUid();

        mRootRef= FirebaseDatabase.getInstance().getReference();
        mRootRef.keepSynced(true);

        mDtbUnread  = FirebaseDatabase.getInstance().getReference().child("Unread_Count").child(mCurrentUserId).child("totalChatCount");
        mDtbUnread.keepSynced(true);


        mImageStorage = FirebaseStorage.getInstance().getReference();

        //----------CUSTOM ACTION BAR ITEMS-----------------//

        mInfo = findViewById(R.id.imageViewinfo);


        rootView = findViewById(R.id.root_view);
        emojiconEditText = findViewById(R.id.emojicon_edit_text);


        emojiconEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {




            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()<=0){

                    mChatSendBtn.setVisibility(View.VISIBLE);
                    mChatSendBtn.setImageResource(R.drawable.ic_right_arrow);
                }else{
                    mChatSendBtn.setVisibility(View.VISIBLE);
                    mChatSendBtn.setImageResource(R.drawable.ic_right_arrow);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

                //mChatSendBtn.setImageResource(R.drawable.ic_send_blue_24dp);

            }
        });

        mChatSendBtn = findViewById(R.id.chat_send_btn);
        mChatMessageView = findViewById(R.id.chat_message_view);

        // ------------------------SINCH CALLING APP TO APP ----------------------------



        mAdapter = new MessageAdapter(messagesList);

        mMessagesList = (RecyclerView) findViewById(R.id.messages_list);

        mLinearLayout = new LinearLayoutManager(this);
        mMessagesList.setHasFixedSize(true);
        mMessagesList.setLayoutManager(mLinearLayout);

        mMessagesList.setAdapter(mAdapter);

        loadMessages();

        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else {
            connected = false;


        }
        mRootRef.child("Chat").child(mCurrentUserId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.exists()){

                    Map chatAddMap = new HashMap();
                    chatAddMap.put("seen", false);
                    chatAddMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatUserMap = new HashMap();
                    chatUserMap.put("Chat/" + mCurrentUserId + "/" ,chatAddMap);

                    mRootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if (databaseError != null){

                                Log.d("CHAT_LOG",databaseError.getMessage().toString());

                            }

                        }
                    });

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });





        //---------------------BTN CLICK-------------------//


        mChatSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendMessage();

            }
        });





    }

    private void loadMessages() {

        mRootRef.child("messages").child(mCurrentUserId).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (dataSnapshot.exists()){
                    Messages message = dataSnapshot.getValue(Messages.class);

                    messagesList.add(message);
                    mAdapter.notifyDataSetChanged();

                    mMessagesList.scrollToPosition(messagesList.size()-1);
                }


            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void sendMessage() {

        String message = emojiconEditText.getText().toString().trim();

        if (!TextUtils.isEmpty(message)){



            //mChatSendBtn.setImageResource(R.drawable.ic_send_blue_24dp);

            String current_user_ref = "messages/" + mCurrentUserId;

            DatabaseReference user_message_push = mRootRef.child("messages").child(mCurrentUserId).push();


            final String push_id = user_message_push.getKey();

            final DatabaseReference mChat_orderT = mRootRef.child("Chat_Order_Time").child(mCurrentUserId);
            final DatabaseReference test = mRootRef.child("messages").child(mCurrentUserId);
            final DatabaseReference mUnreadCount = mRootRef.child("Unread_Count");
            final DatabaseReference mChatCount = mRootRef.child("Unread_Chat_Count").child(mCurrentUserId);

            mUnreadCount.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()){

                        String chatCount = (String) dataSnapshot.child("totalChatCount").getValue();
                        Integer increment = Integer.valueOf(chatCount);

                        Map messageMapCountUnread = new HashMap();
                        messageMapCountUnread.put("totalChatCount", String.valueOf(increment + 1));

                        mUnreadCount.updateChildren(messageMapCountUnread, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if (databaseError != null){

                                    Log.d("CHAT_LOG",databaseError.getMessage().toString());

                                }

                            }
                        });


                    }else {


                        Map messageMapCountUnread = new HashMap();
                        messageMapCountUnread.put("totalChatCount", "1");

                        mUnreadCount.updateChildren(messageMapCountUnread, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if (databaseError != null){

                                    Log.d("CHAT_LOG",databaseError.getMessage().toString());

                                }

                            }
                        });

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            mChatCount.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.exists()){

                        String chatCount = (String) dataSnapshot.child("totalChatCount").getValue();
                        Integer increment = Integer.valueOf(chatCount);

                        Map messageMapCountUnread = new HashMap();
                        messageMapCountUnread.put("totalChatCount", String.valueOf(increment+1));
                        messageMapCountUnread.put("seen", "No");

                        mChatCount.updateChildren(messageMapCountUnread, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if (databaseError != null){

                                    Log.d("CHAT_LOG",databaseError.getMessage().toString());

                                }

                            }
                        });


                    }else {


                        Map messageMapCountUnread = new HashMap();
                        messageMapCountUnread.put("totalChatCount", "1");
                        messageMapCountUnread.put("seen", "No");

                        mChatCount.updateChildren(messageMapCountUnread, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                                if (databaseError != null){

                                    Log.d("CHAT_LOG",databaseError.getMessage().toString());

                                }

                            }
                        });

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });


            final String currentDate = java.text.DateFormat.getDateTimeInstance().format(new Date());
            final String simpledate = new SimpleDateFormat("dd,MMMM,yyyy").format(new Date());
            final String simpledate2 = new SimpleDateFormat("EEE, MMM d, ''yy").format(new Date());

            final DatabaseReference newNotificationref = mRootRef.child("notifications_message").push();
            String newNotificationId = newNotificationref.getKey();

            final Map notificationData = new HashMap<>();
            notificationData.put("from",mCurrentUserId);
            notificationData.put("message",message);
            notificationData.put("type","message_sent");



            newNotificationref.updateChildren(notificationData, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                }
            });


            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("type", "text");
            messageMap.put("time", simpledate);
            messageMap.put("Ordertime", ServerValue.TIMESTAMP);
            messageMap.put("message_key", push_id);
            messageMap.put("from", mCurrentUserId);
            messageMap.put("to", "Bot");

            Map messageUserMap = new HashMap();
            messageUserMap.put(current_user_ref + "/" + push_id, messageMap);

            emojiconEditText.setText("");

            //--------------------ME--------------------

            mChat_orderT.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if (dataSnapshot.hasChild(simpledate)){

                    }else {

                        mChat_orderT.child(simpledate).child("OrderTime").setValue(ServerValue.TIMESTAMP);
                        mChat_orderT.child(simpledate).child("Display").setValue(simpledate2);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            //--------------------OTHER-------------------------


            mRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if (databaseError != null){

                        Log.d("CHAT_LOG",databaseError.getMessage());
                        Toast.makeText(ChatActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            });




        }else {

            String errorMessage;

            errorMessage = "Cannot send an empty message";

            //Snackbar.make(findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_SHORT).show();

        }


    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser==null){

        }else {

            mUserRef.child("online").setValue("true");

        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser!=null) {

            mUserRef.child("online").setValue(ServerValue.TIMESTAMP);

        }

    }
}
