package com.mobile.thamaniukulimaapp;

/**
 * Created by Lenovo on 15/09/2017.
 */

public class Messages {

    private String message, type, from, to, time;
    private boolean seen;
    private long Ordertime;

    public Messages (String message, boolean seen, String time, String type, String from, String to, long Ordertime){

        this.message = message;
        this.seen = seen;
        this.time = time;
        this.type = type;
        this.from = from;
        this.to = to;
        this.Ordertime = Ordertime;
    }

    public String getMessage(){
        return message;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public boolean getSeen(){
        return seen;
    }
    public void setSeen(boolean seen){
        this.seen = seen;
    }
    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time = time;
    }
    public String getType(){
        return type;
    }
    public void setType(String type){
        this.type = type;
    }
    public String getFrom(){
        return from;
    }
    public void setFrom(String from){
        this.from = from;
    }
    public String getTo(){
        return to;
    }
    public void setTo(String to){
        this.to = to;
    }
    public long getOrdertime(){
        return Ordertime;
    }
    public void setOrdertime(long Ordertime){
        this.Ordertime = Ordertime;
    }

    public Messages(){

    }
}
