package com.mobile.thamaniukulimaapp;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.cocosw.bottomsheet.BottomSheet;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;
import com.mobile.thamaniukulimaapp.Common.Common;
import com.mobile.thamaniukulimaapp.MainFragments.CalenderFragment;
import com.mobile.thamaniukulimaapp.MainFragments.ForumsFragment;
import com.mobile.thamaniukulimaapp.MainFragments.HomeFragment;
import com.mobile.thamaniukulimaapp.MainFragments.MapsFragment;
import com.mobile.thamaniukulimaapp.MainFragments.NotificationFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private Toolbar mToolbar;
    private TextView mTxtTitle;
    private ImageButton imgBtnSort, imgBtnMap, imgBtnBack;
    private CircleImageView mImgProf;
    private FirebaseAuth mAuth;
    private DatabaseReference mCustomerDatabase, mTaskDB, mDtbUser;;
    private Query mQueryTaskHist;
    private String userID = Common.getUserID();


    private ViewPager viewPager;
    private TabLayout tabs;

    private SpaceNavigationView bottomNavigation;
    //private LinearLayout lnBottomSheet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);

        mDtbUser = FirebaseDatabase.getInstance()
                .getReference().child("Users").child(userID);
        mDtbUser.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (!dataSnapshot.child("MyFarms").exists()) {
                    startActivity(new Intent(MainActivity.this, AddFarmActivity.class));
                    finish();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mToolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(mToolbar);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
        }
        if (actionBar != null) {
            actionBar.setDisplayShowCustomEnabled(true);
        }
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(R.layout.toolbar_custom_home, null);
        if (actionBar != null) {
            actionBar.setCustomView(action_bar_view);
        }

        viewPager = findViewById(R.id.view_pager);
        //setupViewPager(viewPager);
        //viewPager.setAdapter(sectionsPagerAdapter);
        viewPager.setCurrentItem(0);

        tabs = findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);

        bottomNavigation =  findViewById(R.id.bottom_navigation);
        changeFragment(0);


        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(MainActivity.this, SignInActivity.class));
        }

        mImgProf = findViewById(R.id.imgProf);
        mTxtTitle  = findViewById(R.id.txtTitle);
        mTxtTitle.setText("Home");
        imgBtnMap  = findViewById(R.id.imgBtnMap);
        imgBtnMap.setVisibility(View.VISIBLE);
        imgBtnBack  = findViewById(R.id.imgBtnBack);
        imgBtnBack.setVisibility(View.GONE);
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm=getSupportFragmentManager();
                FragmentTransaction ft=fm.beginTransaction();
                if(fm.getBackStackEntryCount()>0) {
                    fm.popBackStack();
                }else {
                    changeFragment(0);
                }

                ft.commit();

                mImgProf.setVisibility(View.VISIBLE);
                imgBtnMap.setVisibility(View.VISIBLE);
                imgBtnSort.setVisibility(View.VISIBLE);
                imgBtnBack.setVisibility(View.GONE);

            }
        });

        mImgProf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, ProfileActivity.class);
                ActivityOptions options = ActivityOptions
                        .makeSceneTransitionAnimation(MainActivity.this, mImgProf, "robot");
                // start the new activity
                startActivity(intent, options.toBundle());

            }
        });

        imgBtnMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction transaction;
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frameLayout, new MapsFragment());
                transaction.commit();
                transaction.addToBackStack(null);
                imgBtnMap.setVisibility(View.GONE);
                mTxtTitle.setText("Map");
                imgBtnBack.setVisibility(View.VISIBLE);

            }
        });
        imgBtnSort  = findViewById(R.id.imgBtnSort);
        imgBtnSort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showPopup();
            }
        });
//        imgBtnSort.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                //showAlertDialogButtonClicked();
//
//
//            }
//        });

        mTextMessage = findViewById(R.id.message);

        mAuth = FirebaseAuth.getInstance();
        mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child(userID);
        mCustomerDatabase.keepSynced(true);
        mTaskDB = FirebaseDatabase.getInstance().getReference().child("Tasks").child(userID);
        mQueryTaskHist = mTaskDB.orderByChild("Status").equalTo("Incomplete");

        getUserInfo();


        bottomNavigation.initWithSaveInstanceState(savedInstanceState);
        bottomNavigation.addSpaceItem(new SpaceItem("", R.drawable.ic_outline_home_black));
        bottomNavigation.addSpaceItem(new SpaceItem("", R.drawable.ic_baseline_access_time_black));
        bottomNavigation.addSpaceItem(new SpaceItem("", R.drawable.ic_outline_forum_black));
        bottomNavigation.addSpaceItem(new SpaceItem("", R.drawable.ic_outline_notifications_black));
        bottomNavigation.showIconOnly();

        bottomNavigation.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {

                new BottomSheet.Builder(MainActivity.this, R.style.BottomSheet_Dialog)
                        .title("Plant Health")
                        .grid() // <-- important part
                        .sheet(R.menu.classification)
                        .listener(new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case R.id.action_bean:
                                        startActivity(new Intent(MainActivity.this, ImageClassificationActivity.class));
                                        break;
                                }
                            }
                        }).show();
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {

                if (itemIndex==0){

                    changeFragment(0);

                    mTxtTitle.setText("Home");
                    mTxtTitle.setVisibility(View.VISIBLE);
                    imgBtnSort.setVisibility(View.VISIBLE);
                    imgBtnBack.setVisibility(View.GONE);
                    imgBtnMap.setVisibility(View.VISIBLE);
                    mImgProf.setVisibility(View.VISIBLE);

                }
                if (itemIndex==1){

                    changeFragment(1);

                    mTxtTitle.setText("Planning");
                    mTxtTitle.setVisibility(View.VISIBLE);
                    imgBtnSort.setVisibility(View.VISIBLE);
                    imgBtnBack.setVisibility(View.GONE);
                    imgBtnMap.setVisibility(View.GONE);
                    mImgProf.setVisibility(View.VISIBLE);

                }
                if (itemIndex==2){

                    changeFragment(2);

                    mTxtTitle.setText("Forum");
                    mTxtTitle.setVisibility(View.VISIBLE);
                    imgBtnSort.setVisibility(View.VISIBLE);
                    imgBtnBack.setVisibility(View.GONE);
                    imgBtnMap.setVisibility(View.GONE);
                    mImgProf.setVisibility(View.VISIBLE);


                }
                if (itemIndex==3){

                    changeFragment(3);

                    mTxtTitle.setText("Notifications");
                    mTxtTitle.setVisibility(View.VISIBLE);
                    imgBtnSort.setVisibility(View.VISIBLE);
                    imgBtnBack.setVisibility(View.GONE);
                    imgBtnMap.setVisibility(View.GONE);
                    mImgProf.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {

            }
        });


        AHBottomNavigationItem item1 = new AHBottomNavigationItem("Home", R.drawable.ic_icon_feather_home);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem("Planning", R.drawable.ic_icon_feather_calendar);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem("Explore", R.drawable.ic_icon_feather_compass);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem("Notifications", R.drawable.ic_icon_feather_bell);


//        bottomNavigation.addItem(item1);
//        bottomNavigation.addItem(item2);
//        bottomNavigation.addItem(item4);
//        bottomNavigation.addItem(item5);
//        bottomNavigation.setCurrentItem(0);
//        bottomNavigation.setBehaviorTranslationEnabled(true);
//        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#ffffff"));
//        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_HIDE);
//        bottomNavigation.setAccentColor(Color.parseColor("#000000"));//128C7E
//        bottomNavigation.setInactiveColor(Color.parseColor("#7e7e7e"));
//        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#ff0000"));


        mQueryTaskHist.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                if (dataSnapshot.exists()){

                    String strTaskCount = String.valueOf(dataSnapshot.getChildrenCount());
                    //bottomNavigation.showBadgeAtIndex(1, (int) dataSnapshot.getChildrenCount(), Color.parseColor("#ff0000"));
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void getUserInfo() {

        mCustomerDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();


                    if(map.get("profileImageUrl")!=null){
                        String mProfileImageUrl = (String) map.get("profileImageUrl");

                        Picasso.get().load(mProfileImageUrl).networkPolicy(NetworkPolicy.OFFLINE)
                                .placeholder(R.drawable.userprof).into(mImgProf, new Callback() {
                            @Override
                            public void onSuccess() {

                            }

                            @Override
                            public void onError(Exception e) {

                                Picasso.get().load(mProfileImageUrl).placeholder(R.drawable.userprof).into(mImgProf);

                            }



                        });
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

//        mTxtTitle.setText("Home");
//        imgBtnMap.setVisibility(View.VISIBLE);
//        imgBtnSort.setVisibility(View.VISIBLE);
//        imgBtnBack.setVisibility(View.GONE);
    }

    private long ls, le, se, pe, fe, we, he;
    private long add_lp ,add_ss, add_p, add_f, add_w, add_h;

    public void showAlertDialogButtonClicked() {

        //String randID = UUID.randomUUID().toString();
        String randID = mTaskDB.push().getKey();
        Date date = new Date();
        ls = date.getTime();

        // custom layout
        final View customLayout = getLayoutInflater().inflate(R.layout.task_main_layout, null);
        CalendarView calendar = customLayout.findViewById(R.id.calendarView);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            //show the selected date as a toast
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
                Toast.makeText(getApplicationContext(), day + "/" + month + "/" + year, Toast.LENGTH_SHORT).show();
                Calendar c = Calendar.getInstance();
                c.set(year, month, day);
                ls = c.getTimeInMillis(); //this is what you want to use later
                //ls = calendar.getDate();
                le = ls + 864000000;
                se = le + 86400000;
                pe = se + 604800000;
                fe = pe + 1814400000;
                we = fe + 2419200000L;
                he = we + 86400000;
            }
        });
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Farm Planner");
        builder.setView(customLayout);

        // add a button
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // ----------------- POST TASK TO DB -----------------------
                EditText mEdtTask = customLayout.findViewById(R.id.edtTaskMain);
                String task = mEdtTask.getText().toString().trim();

                mTaskDB.child(randID).child("Crop").setValue("Maize");
                mTaskDB.child(randID).child("ls").setValue(ls);
                mTaskDB.child(randID).child("le").setValue(le);
                mTaskDB.child(randID).child("se").setValue(se);
                mTaskDB.child(randID).child("pe").setValue(pe);
                mTaskDB.child(randID).child("fe").setValue(fe);
                mTaskDB.child(randID).child("we").setValue(we);
                mTaskDB.child(randID).child("he").setValue(he);
                mTaskDB.child(randID).child("Status").setValue("Incomplete").addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Toast.makeText(MainActivity.this, "Farming Planned", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();

            }
        });

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showPopup() {
        PopupMenu popup = new PopupMenu(this, imgBtnSort);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.services, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
//                    case R.id.action_admin:
//                        startActivity(new Intent(MainActivity.this, AdminActivity.class));
//                        break;
                    case R.id.action_sale:
                        startActivity(new Intent(MainActivity.this, AddFarmActivity.class));
                        break;
                    case R.id.action_task:
                        showAlertDialogButtonClicked();
                        break;
                }

                return true;
            }
        });

        popup.show();
    }

    private void changeFragment (int position){

        FragmentTransaction transaction;
        transaction = getSupportFragmentManager().beginTransaction();

        if (position == 0){
            transaction.replace(R.id.frameLayout, new HomeFragment());
            transaction.commit();
        }

        if (position == 1){
            transaction.replace(R.id.frameLayout, new CalenderFragment());
            transaction.commit();
        }

        if (position == 2){
            transaction.replace(R.id.frameLayout, new ForumsFragment());
            transaction.commit();
        }
        if (position == 3){
            transaction.replace(R.id.frameLayout, new NotificationFragment());
            transaction.commit();
        }
    }

    private void setupViewPager(ViewPager viewPager) {


        Adapter adapter = new Adapter(getSupportFragmentManager());
//        adapter.addFragment(new TodayWeatherFragment(), "Today");
//        adapter.addFragment(new FoodSafetyFragment(), "Info");
        //adapter.addFragment(new MyFarmsFragment(), "Farms");
        viewPager.setAdapter(adapter);



    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}

